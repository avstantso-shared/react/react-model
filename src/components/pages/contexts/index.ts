import * as _List from './list';

export const Contexts = {
  List: _List.Context,
  useList: _List.useContext,
};
