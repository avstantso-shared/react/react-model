import React from 'react';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import { List } from '../controllers';

const empty: List<any> = {
  declaration: null,
  modelStorage: null,
  asyncAction: null,
  showActive: null,
  setShowActive: null,
  pathname: null,
  isInPathname: null,
  action: null,
  readOnly: null,
  inTrashBinRecs: null,
  visible: null,
  inTrash: null,
  noOutlet: null,
};

export const Context = React.createContext<List<any>>(empty);
export const useContext = <TModel extends Model.Structure = any>() =>
  React.useContext(Context) as List<TModel>;
