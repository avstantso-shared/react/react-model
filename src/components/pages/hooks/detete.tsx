import { useLocation, useNavigate } from 'react-router';
import { validate as isValidUUID } from 'uuid';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';
import { Dialogs, ErrorBox } from '@avstantso/react--components';

import type { EntityName, ModelStorage } from '@types';
import { Trans, LOCALES, useTranslation } from '@i18n';
import { useToasts } from '@toasts';
import { useAxiosEffect } from '@hooks';
import { useHistory } from '@history';
import { UnableLoadError } from '@classes';

import { calcBackUrl, calcCancelUrl } from './utils';

export namespace DeleteEntity {
  export interface Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.ReadableWritable<TModel>
  > extends ModelStorage.ReadableWritable.Provider<TModel, TModelStorage> {
    entity: Model.ID | TModel['Select'];
    entityName?: EntityName.Union;
    message?: (entity: TModel['Select']) => string | JSX.Element;
    backUrl?: string;
  }
}

export function useDeleteEntity<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
>(props: DeleteEntity.Props<TModel, TModelStorage>): void {
  const location = useLocation();
  const navigate = useNavigate();
  const { queue } = useHistory();

  const { modelStorage, message, entityName } = props;

  const { deleteDialog } = Dialogs.Common.use();
  const { entityIdIsActioned } = useToasts();

  const [t, i18n] = useTranslation();

  const toaster = ErrorBox.useToaster();

  const id = JS.is.string(props.entity) ? props.entity : props.entity?.id;

  useAxiosEffect(({ config }) => {
    const backUrl = props.backUrl || calcBackUrl(location);

    const cancel = () =>
      navigate(calcCancelUrl(queue, id, backUrl), { state: { location } });

    if (!isValidUUID(id)) {
      toaster(UnableLoadError.invalidId(id));
      return cancel();
    }

    async function execute() {
      try {
        const entity = await modelStorage.one(id, config);

        if (Model.Systemic.Force(entity).system) {
          toaster.warn(UnableLoadError.systemDataChangeForbidden(id));
          return cancel();
        }

        const ok = await deleteDialog.execute(
          <>
            <Trans.N
              {...{ t, i18n }}
              i18nKey={LOCALES.doYouReallyWantToDelete}
            />{' '}
            {message ? message(entity) : ''}?
          </>
        );

        if (!ok) return cancel();

        let action: UrlActions.Union;
        if (Model.Activable.Is(entity) && entity.active) {
          await modelStorage.update({ ...entity, active: false }, config);
          action = +UrlActions.Trash | +UrlActions.Edit;
        } else {
          await modelStorage.delete(id, config);
          action = UrlActions.Delete;
        }

        entityIdIsActioned(id, action, entityName);

        navigate(backUrl, { state: { location } });
      } catch (e) {
        toaster(e);
      }
    }

    execute();

    return () => {
      deleteDialog.close();
    };
  }, []);
}

export function DeleteEntity<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
>(props: DeleteEntity.Props<TModel, TModelStorage>): JSX.Element {
  useDeleteEntity(props);
  return null;
}
