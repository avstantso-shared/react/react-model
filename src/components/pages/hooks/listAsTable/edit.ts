import React, { useCallback } from 'react';
import { useNavigate } from 'react-router';

import { Perform } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

export type EditHandler<TModel extends Pick<Model.Structure, 'Select'>> = (
  entity: TModel['Select']
) => () => void;

export const useEdit = <TModel extends Pick<Model.Structure, 'Select'>>(
  rootUrl: string,
  editable?: Perform<boolean, [TModel['Select']]>,
  inTrash?: boolean
): EditHandler<TModel> => {
  const navigate = useNavigate();

  return useCallback(
    (entity) => () => {
      const isEditable: boolean =
        undefined === editable || Perform(editable, entity);

      navigate(
        UrlActions.editOrSystemicView(rootUrl, entity, isEditable, inTrash)
      );
    },
    [rootUrl, editable, inTrash]
  );
};
