import { useLocation, useNavigate } from 'react-router';
import { validate as isValidUUID } from 'uuid';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { UrlActions } from '@avstantso/react--data';
import { ErrorBox } from '@avstantso/react--components';

import type { EntityName, ModelStorage } from '@types';
import { useToasts } from '@toasts';
import { useAxiosEffect } from '@hooks';
import { useHistory } from '@history';
import { UnableLoadError } from '@classes';

export namespace RestoreEntity {
  export interface Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.ReadableWritable<TModel>
  > extends ModelStorage.ReadableWritable.Provider<TModel, TModelStorage> {
    entity: Model.ID | TModel['Select'];
    entityName?: EntityName.Union;
    backUrl?: string;
    action?: UrlActions;
  }
}

export function useRestoreEntity<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
>(props: RestoreEntity.Props<TModel, TModelStorage>): void {
  const location = useLocation();
  const navigate = useNavigate();
  const { queue } = useHistory();

  const { modelStorage, entityName, action } = props;

  const { entityIdIsActioned } = useToasts();

  const toaster = ErrorBox.useToaster();

  const id = JS.is.string(props.entity) ? props.entity : props.entity?.id;

  useAxiosEffect(
    ({ config }) => {
      if (!modelStorage.isReady) return;

      const redirect = (submit?: boolean) => {
        const url =
          props.backUrl ||
          (submit
            ? UrlActions.replaceInPath(
                queue[1].location.pathname,
                +action & (+UrlActions.View | +UrlActions.Edit)
                  ? +action & ~(+UrlActions.Trash | +UrlActions.Restore)
                  : UrlActions.Trash
              )
            : queue.length > 1
            ? queue[1].location.pathname
            : UrlActions.replaceInPath(location.pathname, UrlActions.Root));

        navigate(url, {
          replace: true,
          state: {
            location,
            submit,
            label: `Pages.Hooks.${useRestoreEntity.name}`,
          },
        });
      };

      if (!isValidUUID(id)) {
        toaster(UnableLoadError.invalidId(id));
        return redirect();
      }

      async function execute() {
        try {
          const entity = await modelStorage.one(id, config);

          if (!Model.Activable.Is(entity))
            throw new UnableLoadError(`Restore not activatable entity attempt`);

          if (entity.active) {
            toaster(UnableLoadError.cannotRestoreActive(id));
            return redirect();
          }

          if (Model.Systemic.Force(entity).system) {
            toaster.warn(UnableLoadError.systemDataChangeForbidden(id));
            return redirect();
          }

          await modelStorage.update({ ...entity, active: true }, config);

          entityIdIsActioned(id, UrlActions.Restore, entityName);

          redirect(true);
        } catch (e) {
          toaster(e);
        }
      }

      execute();
    },
    [modelStorage.isReady]
  );
}

export function RestoreEntity<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
>(props: RestoreEntity.Props<TModel, TModelStorage>): JSX.Element {
  useRestoreEntity(props);
  return null;
}
