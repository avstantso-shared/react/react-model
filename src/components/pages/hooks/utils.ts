import { To, Location } from 'react-router';
import { validate as isValidUUID } from 'uuid';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

import { useHistory } from '@history';

export function calcBackUrl(location: string | Location): string {
  const pathname = JS.is.string(location) ? location : location.pathname;

  const pathnameArr = pathname.replace(/\/$/, '').split('/');
  if (pathnameArr.length <= 2) return null;

  const removed = pathnameArr.splice(pathnameArr.length - 2, 2);
  if (
    !UrlActions.stringIs(removed[1], UrlActions.Delete, UrlActions.Restore) ||
    !isValidUUID(removed[0])
  )
    return null;

  return pathnameArr.join('/');
}

export function calcCancelUrl(
  queue: ReturnType<typeof useHistory>['queue'],
  id: Model.ID,
  backUrl: string
): To {
  for (let i = 0; i < queue.length; i++) {
    const [tail, prevTail] = queue[i].location.pathname.split('/').reverse();
    if (id === tail || (id === prevTail && UrlActions.stringIs.Edit(tail)))
      return queue[i].location;

    if (
      prevTail === id &&
      UrlActions.stringIs(tail, UrlActions.Delete, UrlActions.Restore)
    )
      continue;

    break;
  }

  return backUrl;
}
