import React from 'react';

import { Combiner, InnerRef } from '@avstantso/react--data';
import { AsyncAction, Dialogs } from '@avstantso/react--components';

import { PageBaseViewHeader } from './header';

import './base.scss';

export namespace PageBaseView {
  export namespace Header {
    export type GetDynamic = PageBaseViewHeader.GetDynamic;

    export namespace Props {
      export type External = PageBaseViewHeader.Props.External;
    }

    export type Props = PageBaseViewHeader.Props;
  }

  export type Props = Dialogs.Sub.Props &
    InnerRef.Provider<HTMLDivElement> &
    Partial<AsyncAction.Readonly.Provider> &
    React.ComponentProps<'section'> & {
      readonly isCompleteState?: boolean;
      readonly header?: PageBaseView.Header.Props | React.ReactNode;
      readonly footer?: React.ReactNode;
      readonly asFragment?: boolean;
    };

  export type FC = React.FC<Props>;

  export namespace FC {
    export type Ex = FC & { Header: PageBaseViewHeader.FC };
  }
}

export const PageBaseView: PageBaseView.FC.Ex = (props) => {
  const {
    dialogs,
    innerRef,
    children,
    asFragment,
    className,
    header,
    footer = null,
    asyncAction: { loading, Boundary } = AsyncAction.staticReadonly,
    isCompleteState = !loading,
    ...rest
  } = props;

  if (asFragment)
    return (
      <>
        <Dialogs.Sub {...{ dialogs }} />
        {children}
      </>
    );

  const override =
    undefined === isCompleteState ? undefined : loading || !isCompleteState;

  return (
    <section
      {...Combiner.className('page-view', className)}
      {...rest}
      ref={innerRef}
    >
      <Dialogs.Sub {...{ dialogs }} />
      {!!header && (
        <PageBaseViewHeader
          {...PageBaseView.Header.formalize(header)}
          {...{ isCompleteState }}
        />
      )}
      <Boundary.ErrorBox hr />
      <div className="content">
        <Boundary.Loading.Text {...{ override }}>
          {children}
        </Boundary.Loading.Text>
      </div>
      {isCompleteState && footer}
    </section>
  );
};

PageBaseView.Header = PageBaseViewHeader;
