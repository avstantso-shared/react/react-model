import React from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { BackIconLink, Loading } from '@avstantso/react--components';

export namespace PageBaseViewHeader {
  export type GetDynamic = (isCompleteState: boolean) => React.ReactNode;

  export namespace Props {
    export namespace External {
      export type Extend = () => React.ReactNode | React.ReactNode[];
    }
    export type External = { extend?: External.Extend };
  }

  export type Props = Props.External & {
    static?: React.ReactNode;
    dynamic?: PageBaseViewHeader.GetDynamic;
    backUrl?: string;
  };

  export type FC = React.FC<Props & { isCompleteState?: boolean }> & {
    formalize(header: Props | React.ReactNode): Props;
  };
}

export const PageBaseViewHeader: PageBaseViewHeader.FC = (props) => {
  const {
    isCompleteState,
    static: staticHeader,
    dynamic: dynamicHeader,
    backUrl,
    extend,
  } = props;

  const dynH = dynamicHeader ? dynamicHeader(isCompleteState) : null;

  return (
    (staticHeader || dynH) && (
      <header>
        <h2>
          {backUrl && <BackIconLink url={backUrl} />}
          {staticHeader}
          {!isCompleteState && <Loading />} {dynH}
        </h2>
        {extend && extend()}
      </header>
    )
  );
};

PageBaseViewHeader.formalize = (header) =>
  !header
    ? {}
    : JS.is.object(header) && !React.isValidElement(header)
    ? header || {}
    : { static: header };
