import React from 'react';

import { Model } from '@avstantso/node-or-browser-js--model-core';
import { Combiner } from '@avstantso/react--data';

import { Contexts } from '../../contexts';
import type { List } from '../../controllers';

import { PageBaseView } from '../base';
import { ControlledPageView } from '../controlled';

import { TrashBin } from './trashBin';
import { IterableProgress } from './iterable-progress';
import { NoIterablePagination } from './no-iterable-pagination';
import './list.scss';

export namespace ListPageView {
  export type Props<TModel extends Model.Structure> = Omit<
    ControlledPageView.Props<TModel, List<TModel>>,
    'isCompleteState'
  >;
}

export function ListPageView<TModel extends Model.Structure>(
  props: ListPageView.Props<TModel>
) {
  const { children, className, ...rest } = props;
  const { controller } = props;
  const {
    pagination,
    asyncAction: { loading },
  } = controller;

  return (
    <Contexts.List.Provider value={controller}>
      <ControlledPageView<TModel, List<TModel>>
        {...rest}
        {...Combiner.className('page-view-list', className)}
        isCompleteState={!loading}
      >
        {!!pagination?.iterable && <IterableProgress {...props} />}
        {children}
        {!!(pagination && !pagination.iterable) && (
          <NoIterablePagination {...props} />
        )}
      </ControlledPageView>
    </Contexts.List.Provider>
  );
}

ListPageView.TrashBin = TrashBin;
