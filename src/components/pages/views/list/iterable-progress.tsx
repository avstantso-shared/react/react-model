import React from 'react';
import { ProgressBar } from 'react-bootstrap';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { ModelStorage } from '@types';

import * as Types from './types';

export const IterableProgress: React.FC<Types.Props<any>> = (props) => {
  const {
    controller: { modelStorage },
  } = props;

  const collection = (modelStorage as ModelStorage.Readable<any>).collection;

  const now =
    (collection.length /
      (JS.is.number(collection.total) ? collection.total : collection.length)) *
      100 || 0;

  return (
    <ProgressBar
      className="pages-progress"
      style={now >= 100 ? { display: 'none' } : {}}
      animated
      variant="success"
      now={now}
      label={`${Math.round(now * 100) / 100}%`}
    />
  );
};
