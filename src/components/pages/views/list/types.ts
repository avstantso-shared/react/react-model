import type React from 'react';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import type { List } from '../../controllers';

import { ControlledPageView } from '../controlled';

export type Props<TModel extends Model.Structure> = ControlledPageView.Props<
  TModel,
  List<TModel>
>;
