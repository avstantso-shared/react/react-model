import React from 'react';
import { useLocation, useNavigate } from 'react-router';
import { Pagination } from 'react-bootstrap';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';

import { ModelStorage } from '@types';

import * as Types from './types';

export const NoIterablePagination: React.FC<Types.Props<any>> = (props) => {
  const {
    controller: { pagination, modelStorage },
  } = props;

  const location = useLocation();
  const navigate = useNavigate();
  const searchParams = new URLSearchParams(location.search);

  const collection = (modelStorage as ModelStorage.Readable<any>).collection;

  const size = (() => {
    let r = Number.parseInt(searchParams.get('pageSize'), 10);
    if (isNaN(r)) r = pagination.size;
    if (r <= 0)
      throw new InvalidArgumentError(`pagination.size must be greater than 0`);
    return r;
  })();

  const last = Math.trunc(collection.total / size);

  const num = (() => {
    const r = Number.parseInt(searchParams.get('pageNum'), 10) - 1 || 0;
    if (r < 0) return 0;
    if (r > last) return last;
    return r;
  })();

  function go(newNum: number) {
    const sp = new URLSearchParams(location.search);
    sp.set('pageNum', `${newNum + 1}`);
    sp.set('pageSize', `${size}`);

    // A.V.Stantso:
    // react-bootstrap Pagination do not supports replace <a> to <Link>
    // We must use onClick + e.preventDefault + navigate
    const onClick: React.MouseEventHandler = (e) => {
      newNum !== num &&
        navigate(
          { ...location, search: sp.toString() },
          { state: { location } }
        );

      e.preventDefault();
      e.stopPropagation();
    };

    // A.V.Stantso:
    // for beauty
    const href = `${location.pathname}?${sp.toString()}${
      location.hash ? `#${location.hash}` : ''
    }`;

    return { onClick, href };
  }

  const PGI: React.FC<{ offset?: number }> = ({ offset }) => {
    if (
      JS.is.number(offset) &&
      (offset < 0 ? num < Math.abs(offset) : num > last - offset)
    )
      return null;

    return (
      <Pagination.Item {...go(num + (offset || 0))} active={!offset}>
        {num + (offset || 0) + 1}
      </Pagination.Item>
    );
  };

  return (
    <Pagination size="sm">
      <Pagination.First {...go(0)} disabled={0 === num} />
      <Pagination.Prev {...go(num - 1)} disabled={0 === num} />
      {num > 4 ? (
        <>
          <Pagination.Item {...go(0)}>{1}</Pagination.Item>
          <Pagination.Ellipsis disabled />
        </>
      ) : (
        <>
          <PGI offset={-4} />
          <PGI offset={-3} />
        </>
      )}

      <PGI offset={-2} />
      <PGI offset={-1} />
      <PGI />
      <PGI offset={+1} />
      <PGI offset={+2} />

      {num < last - 4 ? (
        <>
          <Pagination.Ellipsis disabled />
          <Pagination.Item {...go(last)}>{last + 1}</Pagination.Item>
        </>
      ) : (
        <>
          <PGI offset={+3} />
          <PGI offset={+4} />
        </>
      )}

      <Pagination.Next {...go(num + 1)} disabled={last === num} />
      <Pagination.Last {...go(last)} disabled={last === num} />
    </Pagination>
  );
};
