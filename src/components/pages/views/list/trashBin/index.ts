import { TrashSwitch } from './switch';
import { IfInTrash } from './if-in';
import { ResoreFromTrash } from './restore-button';

export namespace TrashBin {
  export interface FC {
    Switch: TrashSwitch.FC;
    IfIn: IfInTrash.FC;
    RestoreButton: ResoreFromTrash.FC;
  }
}

export const TrashBin: TrashBin.FC = {
  Switch: TrashSwitch,
  IfIn: IfInTrash,
  RestoreButton: ResoreFromTrash,
};
