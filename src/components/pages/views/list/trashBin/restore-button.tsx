import React from 'react';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';
import * as Cmps from '@avstantso/react--components';

import type { ModelStorage } from '@types';
import { useToasts } from '@toasts';

import { Contexts } from '../../../contexts';

export namespace ResoreFromTrash {
  export interface Props {
    entity: Model.Select & Model.Activable;
  }

  export type FC = React.FC<Props>;
}

export const ResoreFromTrash: ResoreFromTrash.FC = ({ entity }) => {
  const controller = Contexts.useList();
  const { modelStorage } = controller;

  const { entityIdIsActioned } = useToasts();
  const [t] = Cmps.useTranslation();

  const toaster = Cmps.ErrorBox.useToaster();

  const ms = modelStorage as ModelStorage.ReadableWritable<Model.Structure>;

  const onClick = () =>
    ms
      .update(Generics.Cast.To({ ...entity, active: true }))
      .then((fromServer) => {
        entityIdIsActioned(fromServer.id, UrlActions.Edit);
      }, toaster);

  return (
    <Cmps.Buttons.Small.Restore
      {...{ onClick }}
      title={t(Cmps.LOCALES.buttons.restore)}
    />
  );
};
