import React from 'react';
import { Form } from 'react-bootstrap';

import { Fa } from '@avstantso/react--components';

import { useTranslation, LOCALES } from '@i18n';

import { Contexts } from '../../../contexts';

const { FaTrashRestoreAlt } = Fa;
const LC = LOCALES.trashBin;

export namespace TrashSwitch {
  export type FC = React.FC;
}

export const TrashSwitch: TrashSwitch.FC = () => {
  const controller = Contexts.useList();
  const { inTrashBinRecs, showActive, setShowActive } = controller;
  const [t] = useTranslation();

  const change = () => inTrashBinRecs && setShowActive((prev) => !prev);

  return (
    <div
      className="trash-bin-switch"
      title={t(!inTrashBinRecs ? LC.empty : showActive ? LC.show : LC.hide)}
    >
      <FaTrashRestoreAlt onClick={change} />
      <Form.Check
        type="switch"
        disabled={!inTrashBinRecs}
        checked={!showActive}
        onChange={change}
      />
    </div>
  );
};
