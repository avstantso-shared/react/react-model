import React from 'react';

import { Contexts } from '../../../contexts';

export namespace IfInTrash {
  export type Props = Parameters<typeof Contexts.List.Consumer>[0];

  export type FC = React.FC<Props>;
}

export const IfInTrash: IfInTrash.FC = (props) => (
  <Contexts.List.Consumer {...props} />
);
