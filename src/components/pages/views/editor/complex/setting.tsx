import React from 'react';
import { Badge } from 'react-bootstrap';

import { EditEntity } from '../../../controllers';

export namespace SettingsItem {
  export type Props = EditEntity.Complex.Settings.Item;
  export type FC = React.FC<Props>;
}

export const SettingsItem: SettingsItem.FC = (props) => {
  const { caption, value } = props;

  if (!value) return;

  return (
    <span className="setting-item">
      {caption}
      <Badge color="secondary" className="setting-item-value">
        {value}
      </Badge>
    </span>
  );
};
