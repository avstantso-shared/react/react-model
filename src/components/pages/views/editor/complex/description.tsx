import React from 'react';

import { Trans, LOCALES } from '@i18n';

export namespace Description {
  export type Props = {
    readonly description: string;
  };

  export type FC = React.FC<Props>;
}

export const Description: Description.FC = (props) => {
  const { description } = props;

  return description ? (
    <details>
      <summary>
        <Trans i18nKey={LOCALES.description} />
      </summary>
      <p className="description">{description}</p>
    </details>
  ) : null;
};
