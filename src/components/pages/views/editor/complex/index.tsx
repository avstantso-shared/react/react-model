import React from 'react';
import { ButtonGroup } from 'react-bootstrap';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Buttons } from '@avstantso/react--components';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Combiner } from '@avstantso/react--data';

import type { EditEntity } from '../../../controllers';

import { PageBaseView } from '../../base';
import { OnePageView } from '../../one';

import { SettingsItem } from './setting';
import { Description } from './description';
import './complex.scss';

export namespace ComplexEditorPageView {
  export namespace Header {
    export type Props = PageBaseView.Header.Props & {
      buttons?(standard: React.ReactNode): React.ReactNode | React.ReactNode[];
    };
  }

  export type Props<TModel extends Model.Structure> = OnePageView.Props.Base<
    TModel,
    EditEntity<TModel>
  > & {
    readonly header?: Header.Props;
    readonly noDescription?: boolean;
  };
}

export function ComplexEditorPageView<TModel extends Model.Structure>(
  props: ComplexEditorPageView.Props<TModel>
) {
  const {
    noDescription,
    children,
    className,
    header: outerHeader,
    ...rest
  } = props;
  const { entity, settings, settingsEdit, readOnly } = props.controller;

  const description =
    !noDescription && Generics.Cast.To<Model.Described>(entity)?.description;

  const header = ((): PageBaseView.Header.Props => {
    const formalHeader = PageBaseView.Header.formalize(
      outerHeader
    ) as ComplexEditorPageView.Header.Props;

    return {
      ...formalHeader,
      extend: () => {
        const standardButtons = !readOnly && (
          <Buttons.Small.Edit onClick={settingsEdit} disabled={!settingsEdit} />
        );

        const buttons = formalHeader.buttons
          ? formalHeader.buttons(standardButtons)
          : standardButtons;

        return (
          <>
            {settings &&
              (!Array.isArray(settings)
                ? settings
                : settings
                    .filter(({ value }) => value)
                    .map((item, index) => (
                      <React.Fragment key={index}>
                        {index > 0 && <>&nbsp;&nbsp;&nbsp;&nbsp;</>}
                        <SettingsItem {...item} />
                      </React.Fragment>
                    )))}
            {description && <Description description={description} />}
            {buttons && (
              <ButtonGroup className="buttons">{buttons}</ButtonGroup>
            )}
          </>
        );
      },
    };
  })();

  const footer = OnePageView.Footer.Replace.Null;

  return (
    <OnePageView<TModel, EditEntity<TModel>>
      {...rest}
      {...Combiner.className(
        'page-view-complex-editor',
        readOnly && 'page-view-complex-editor-readonly',
        className
      )}
      {...{ header, footer }}
    >
      {children}
    </OnePageView>
  );
}
