import React from 'react';
// TODO: https://reactrouter.com/docs/en/v6/upgrading/v5#prompt-is-not-currently-supported
// import { Prompt } from 'react-router-dom';

import { Override } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Combiner, UrlActions } from '@avstantso/react--data';
import {
  Buttons,
  MDFDebugger,
  EntityLocalStorage,
} from '@avstantso/react--components';

import type { EditEntity } from '../../../controllers';
import { OnePageView } from '../../one';

export namespace EditorPageView {
  export interface Props<TModel extends Model.Structure>
    extends OnePageView.Props.Base<TModel, EditEntity<TModel>> {
    hideApply?: boolean;
    visibleActions?: UrlActions.Union;
  }
}

export function EditorPageView<TModel extends Model.Structure>(
  props: EditorPageView.Props<TModel>
) {
  const {
    hideApply,
    children,
    className,
    visibleActions = +UrlActions.New | +UrlActions.Edit,
    ...rest
  } = props;
  const { mdf, debug, formik, submit, action, localData } = props.controller;

  const visible: Override.Union<boolean> =
    props.visible ||
    (({ original }) => original && !!(+action & +visibleActions));

  const buttonProps = (
    accept: boolean,
    id = `${accept ? 'save' : 'apply'}-entity`
  ) => ({
    key: id,
    onClick: () => submit(accept),
    id,
    disabled: formik.isSubmitting || !mdf.modified,
    notResponsive: props.notResponsiveFooter,
  });

  const footer = {
    children: [
      <Buttons.Large.Accept {...buttonProps(true)} />,
      !hideApply && <Buttons.Large.Apply {...buttonProps(false)} />,
    ],
  };

  return (
    <>
      {/* <Prompt {...mdf.prompt} /> */}
      <OnePageView<TModel, EditEntity<TModel>>
        {...rest}
        {...Combiner.className('page-view-editor', className)}
        {...{ visible, footer }}
      >
        <EntityLocalStorage.StoredDataAlert {...localData} />
        {children}
        {debug && (
          <>
            <br />
            <MDFDebugger {...{ mdf }} />
          </>
        )}
      </OnePageView>
    </>
  );
}
