import React from 'react';

import { Override } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Combiner, UrlActions } from '@avstantso/react--data';
import { Buttons, DataContainer } from '@avstantso/react--components';

import type { OneEntity } from '../../controllers';
import { OnePageView } from '../one';

export namespace ViewerPageView {
  export type Props<TModel extends Model.Structure> = OnePageView.Props.Base<
    TModel,
    OneEntity<TModel>
  >;
}

export const ViewerPageView = <TModel extends Model.Structure>(
  props: ViewerPageView.Props<TModel>
) => {
  const { children, className, ...rest } = props;
  const { backUrl, entity, readOnly, inTrash, action } = props.controller;

  const visible: Override.Union<boolean> =
    props.visible ||
    (({ original }) => original && !!(+action & +UrlActions.View));

  const buttonProps = (index: number) => ({
    key: index,
    url: backUrl,
    entity,
  });

  const footer = {
    children: [
      !readOnly && !inTrash && (
        <Buttons.Large.Edit.Router {...buttonProps(0)} />
      ),
      inTrash && <Buttons.Large.Restore.Router {...buttonProps(1)} />,
    ],
  };

  return (
    <OnePageView<TModel, OneEntity<TModel>>
      {...rest}
      {...Combiner.className('page-view-viewer', className)}
      {...{ visible, footer }}
    >
      <DataContainer data={entity}>{children}</DataContainer>
    </OnePageView>
  );
};
