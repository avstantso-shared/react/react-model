import React from 'react';

import { Generic, JS } from '@avstantso/node-or-browser-js--utils';

import { BindFC, Combiner } from '@avstantso/react--data';

import { PageBaseView } from '../base';

export namespace CustomPageView {
  export type Props = PageBaseView.Props;

  export namespace FC {
    export namespace Ext {
      export namespace Bind {
        export type Settings = NodeJS.Dict<Props | string>;

        export type Result<TSettings extends Settings> = {
          [K in keyof TSettings]: FC;
        };
      }

      export type Bind = <TSettings extends Bind.Settings>(
        settings: TSettings
      ) => Bind.Result<TSettings>;
    }
    export type Ext = {
      Bind: Ext.Bind;
    };

    export type Full = FC & Ext;
  }

  export type FC = React.FC<Props>;
}

export const CustomPageView: CustomPageView.FC.Full = ({
  className,
  ...rest
}) => (
  <PageBaseView
    {...rest}
    {...Combiner.className('page-view-custom', className)}
  />
);

CustomPageView.Bind = (settings) => {
  const r: Generic = {};

  Object.entries(settings).forEach(([key, value]) => {
    r[key] = BindFC(
      key,
      CustomPageView,
      JS.is.string(value) ? { className: value } : value
    );
  });

  return r;
};
