import React from 'react';
import { Outlet } from 'react-router';

import { Override, Perform } from '@avstantso/node-or-browser-js--utils';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { OneEntity, List } from '../../controllers';

import { PageBaseView } from '../base';

export namespace ControlledPageView {
  export type Props<
    TModel extends Model.Structure,
    TController extends List<TModel> | OneEntity<TModel>
  > = Omit<PageBaseView.Props, 'backUrl' | 'asyncAction'> & {
    readonly controller: TController;
    readonly visible?: Override.Union<boolean>;
  };
}

export function ControlledPageView<
  TModel extends Model.Structure,
  TController extends List<TModel> | OneEntity<TModel>
>(props: ControlledPageView.Props<TModel, TController>) {
  const { controller, children, visible, header: outerHeader, ...rest } = props;

  const {
    visible: clrlVisible,
    noOutlet,
    backUrl,
    asyncAction,
  } = controller as List<TModel> & OneEntity<TModel>;

  const show = clrlVisible && Override.Calculator(clrlVisible)(visible);

  const header =
    show &&
    (backUrl
      ? { ...PageBaseView.Header.formalize(outerHeader), backUrl }
      : outerHeader);

  return !show ? (
    <Outlet />
  ) : (
    <>
      {!noOutlet && <Outlet />}
      <PageBaseView {...rest} {...{ header, asyncAction }}>
        {Perform(children)}
      </PageBaseView>
    </>
  );
}
