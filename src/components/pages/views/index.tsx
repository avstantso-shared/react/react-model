import { PageBaseView as Base } from './base';
import {
  ComplexEditorPageView as ComplexEditor,
  EditorPageView as Editor,
} from './editor';

export { Base, ComplexEditor, Editor };

export { CustomPageView as Custom } from './custom';

export { ListPageView as List } from './list';
export { ListAsTablePageView as ListAsTable } from './listAsTable';
export { ViewerPageView as Viewer } from './viewer';

export namespace Header {
  export type GetDynamic = Base.Header.GetDynamic;

  export namespace Props {
    export type External = Base.Header.Props.External;
  }

  export type Props = Base.Header.Props;

  export type Buttons = ComplexEditor.Header.Props['buttons'];
}

export type Header = Header.Props;
