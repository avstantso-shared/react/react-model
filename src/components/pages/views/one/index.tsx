import React, { useEffect, useRef } from 'react';

import { JS } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Combiner } from '@avstantso/react--data';
import { useDocEventsStack } from '@avstantso/react--components';

import type { OneEntity } from '../../controllers';

import { PageBaseView } from '../base';
import { ControlledPageView } from '../controlled';

import { OnePageViewFooter as Footer } from './footer';
import './one.scss';

export namespace OnePageView {
  export namespace Props {
    export type Base<
      TModel extends Model.Structure,
      TController extends OneEntity<TModel>
    > = Omit<
      ControlledPageView.Props<TModel, TController>,
      'isCompleteState' | 'footer'
    > &
      Pick<Footer.Props, 'notResponsiveFooter'>;
  }

  export type Props<
    TModel extends Model.Structure,
    TController extends OneEntity<TModel>
  > = Omit<PageBaseView.Props, 'footer'> &
    Props.Base<TModel, TController> & {
      footer?: Footer.Props.External;
    };
}

export function OnePageView<
  TModel extends Model.Structure,
  TController extends OneEntity<TModel>
>(props: OnePageView.Props<TModel, TController>) {
  const { className, children, footer, ...rest } = props;
  const { controller } = props;
  const {
    id,
    asyncAction: { loading },
    goBack,
  } = controller;

  const docEventsStack = useDocEventsStack();

  const idRef = useRef<Model.ID>();
  const oldId = idRef.current;
  idRef.current = id;
  const isCompleteState = !loading && (id === oldId || !JS.is.string(oldId));

  useEffect(() => {
    const keyDownHandlerId = docEventsStack.keyDown.add(
      (e: KeyboardEvent): boolean => {
        if ('Escape' === e.code) {
          e.stopPropagation();
          e.stopImmediatePropagation();
          docEventsStack.keyDown.remove(keyDownHandlerId);
          goBack();
          return true;
        }
      }
    );

    return () => {
      docEventsStack.keyDown.remove(keyDownHandlerId);
    };
  }, []);

  return (
    <ControlledPageView<TModel, TController>
      {...Combiner.className('page-view-one', className)}
      {...rest}
      {...{ isCompleteState }}
      footer={<Footer {...props} {...footer} />}
    >
      {children}
    </ControlledPageView>
  );
}

OnePageView.Footer = { Replace: Footer.Replace };
