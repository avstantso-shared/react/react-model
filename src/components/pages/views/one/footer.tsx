import React from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';

import { Buttons } from '@avstantso/react--components';

import { OneEntity } from '../../controllers';

export namespace OnePageViewFooter {
  export type Replace = Perform<React.ReactNode | React.ReactNode[]>;

  export namespace Props {
    export type External = {
      replace?: OnePageViewFooter.Replace;
      children?: React.ReactNode | React.ReactNode[];
    };
  }

  export type Props = {
    controller: OneEntity<any>;
    notResponsiveFooter?: boolean;
  } & Props.External;

  export type FC = React.FC<Props> & {
    Replace: { Null: Props.External };
  };
}

export const OnePageViewFooter: OnePageViewFooter.FC = (props) => {
  const {
    controller: { backUrl, readOnly, entity },
    replace,
    children,
    notResponsiveFooter: notResponsive,
  } = props;

  if (undefined !== replace) {
    const content = Perform(replace);
    return content && <footer>{content}</footer>;
  }

  return (
    <footer>
      <div className="buttons">
        <Buttons.Large.Back.Router url={backUrl} {...{ notResponsive }} />
        <span />
        {!readOnly && (
          <>
            <Buttons.Large.Delete.Router
              url={backUrl}
              entity={entity?.id}
              {...{ notResponsive }}
            />
            <span />
          </>
        )}
        {children}
      </div>
    </footer>
  );
};

OnePageViewFooter.Replace = { Null: { replace: null } };
