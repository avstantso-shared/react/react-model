import React from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';
import { Combiner } from '@avstantso/react--data';
import { Grid } from '@avstantso/react--components';

import type { ListAsTable } from '../../controllers';
import { ListPageView } from '../list';

import './listAsTable.scss';

export namespace ListAsTablePageView {
  export interface Props<
    TModel extends Model.Structure,
    TSelectUnion extends Model.IDed = TModel['Select']
  > extends ListPageView.Props<TModel> {
    controller: ListAsTable<TModel, TSelectUnion>;
    rootUrl: string;
    floatHeader?: boolean;
  }
}

export function ListAsTablePageView<
  TModel extends Model.Structure,
  TSelectUnion extends Model.IDed = TModel['Select']
>(props: ListAsTablePageView.Props<TModel, TSelectUnion>) {
  const { id, controller, rootUrl, children, className, floatHeader, ...rest } =
    props;
  const {
    grid,
    rows,
    asyncAction: { loading },
    customLoading,
  } = controller;

  return (
    <ListPageView
      {...rest}
      {...Combiner.className('page-view-list-in-table', className)}
      {...{ id, controller }}
    >
      <Grid
        responsive
        id={`${id}-grid`}
        data={rows}
        {...{ grid, loading, customLoading, floatHeader }}
      />
      {Perform(children)}
    </ListPageView>
  );
}
