import { useEffect, useState } from 'react';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import type { ModelStorage } from '@types';
import dbg from '@debug';

import { useList } from '../list';
import type { ListAsTable } from './types';

export function useListAsTable<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.Union<TModel>,
  TSelectUnion extends Model.IDed = TModel['Select']
>(
  props: ListAsTable.Props<TModel, TModelStorage, TSelectUnion>
): ListAsTable<TModel, TSelectUnion> {
  const {
    modelStorage,
    filters,
    sorts,
    grid,
    rowsFilter,
    customLoading = null,
  } = props;

  const listController = useList<TModel, TModelStorage>(props);

  const [rows, setRows] = useState<ReadonlyArray<TModel['Select']>>([]);

  // A.V.Stantso: but "useMemo" calculated twice
  useEffect(() => {
    let nextRows = rowsFilter
      ? modelStorage.collection.list.filter(rowsFilter)
      : modelStorage.collection.list;

    nextRows = nextRows.filter(
      (row) =>
        listController.showActive ===
        !!(Model.Activable.Is(row) ? row.active : true)
    );

    nextRows = Generics.Cast(filters.filter(Generics.Cast(nextRows), true));

    sorts.sort(Generics.Cast(nextRows));

    dbg.pages.listAsTable.subNameSpace(modelStorage.name)(
      'rows recalculated %O',
      nextRows
    );

    setRows(nextRows);
  }, [
    modelStorage.collection,
    filters.dataSeria,
    sorts.columns,
    listController.showActive,
  ]);

  return {
    ...listController,
    filters,
    sorts,
    grid,
    rows,
    customLoading,
  };
}
