import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { Filters, Sorts } from '@avstantso/react--data';
import type { DataContainer, Grid } from '@avstantso/react--components';

import type { ModelStorage } from '@types';
import type { List } from '../list';

export namespace ListAsTable {
  export namespace Common {
    export namespace Props {
      export type Optional = Pick<DataContainer.Props, 'customLoading'>;
    }

    export type Props<
      TModel extends Model.Structure,
      TSelectUnion extends Model.IDed = TModel['Select'],
      TOptional extends boolean = true
    > = {
      sorts: Sorts<TSelectUnion>;
      filters: Filters<TSelectUnion>;
      grid: Grid<TSelectUnion>;
    } & (TOptional extends true
      ? Partial<Props.Optional>
      : Required<Props.Optional>);
  }

  export interface Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.Union<TModel>,
    TSelectUnion extends Model.IDed = TModel['Select']
  > extends List.Props<TModel, TModelStorage>,
      Common.Props<TModel, TSelectUnion> {
    rowsFilter?(row: TModel['Select']): boolean;
  }
}

export interface ListAsTable<
  TModel extends Model.Structure,
  TSelectUnion extends Model.IDed = TModel['Select']
> extends List<TModel>,
    ListAsTable.Common.Props<TModel, TSelectUnion, false> {
  rows: ReadonlyArray<TModel['Select']>;
}
