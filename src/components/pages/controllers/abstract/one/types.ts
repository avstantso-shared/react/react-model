import { Perform } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { StateRec } from '@avstantso/react--data';
import { AsyncAction, Dialog } from '@avstantso/react--components';

import { ModelStorage } from '@model';

import type { Base } from '../../types';

export namespace OneEntity {
  export namespace Entity {
    export type FromDlg<TModel extends Model.Structure> =
      Dialog.Entity.Controller<{
        Entity: TModel['Select'];
        Defaults: TModel['Insert'];
      }>;

    export type Raw<TModel extends Model.Structure> = Pick<
      FromDlg<TModel>,
      'entity' | 'setEntityRaw'
    >;
  }

  export type Entity<TModel extends Model.Structure> = StateRec<
    'entity',
    TModel['Select']
  >;

  export namespace External {
    export type Common<TModel extends Model.Structure> = Entity.Raw<TModel>;

    export type Props<TModel extends Model.Structure> = Common<TModel> &
      Pick<Entity.FromDlg<TModel>, 'clacEntity'>;
  }

  export type Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.ReadableWritable<TModel>
  > = ModelStorage.ReadableWritable.Provider<TModel, TModelStorage> &
    Base.Controller.Entity.Props<TModel> & {
      readonly pathname: string;
      readOnly?: Perform<boolean, [TModel['Select']?]>;
    };
}

export type OneEntity<TModel extends Model.Structure> = AsyncAction.Provider &
  Base.Controller.Entity<TModel> &
  OneEntity.External.Common<TModel> &
  OneEntity.Entity<TModel> & {
    readonly backUrl: string;
    goBack(): void;
  };
