import React, { useEffect } from 'react';
import { NavigateFunction, Location } from 'react-router';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

import { cutPathTail } from '../utils';

import type { OneEntity } from './types';

export namespace ActiveTrashRedirect {
  export type Props = Pick<OneEntity<any>, 'pathname' | 'action'> & {
    location: Location;
    navigate: NavigateFunction;
    entity: unknown;
    label: string;
  };
}

export function useActiveTrashRedirect(props: ActiveTrashRedirect.Props): void {
  const { location, navigate, entity, pathname, action, label } = props;

  const { active } = (Model.Activable.Is(entity) && entity) || {};

  useEffect(() => {
    if (
      undefined === active ||
      !action ||
      !(+action & ~(+UrlActions.Root | +UrlActions.Trash | +UrlActions.Restore))
    )
      return;

    function redirect(actionTo: UrlActions.Union) {
      const countToCut =
        (+action & +UrlActions.Trash && 1) +
        (+action & (+UrlActions.View | +UrlActions.Edit | +UrlActions.Delete) &&
          1);

      const url = UrlActions.replaceInPath(
        location.pathname,
        cutPathTail(pathname, countToCut),
        actionTo
      );

      navigate(
        {
          // A.V.Stantso:
          //  All parts of the location except the `pathname` was dropped for safety
          //
          // ...location,
          pathname: url,
        },
        {
          replace: true,
          state: {
            location,
            label: `Pages.Controllers.${label}${useActiveTrashRedirect.name}`,
          },
        }
      );
    }

    if (active === !!(+action & +UrlActions.Trash))
      redirect(
        active ? +action & ~UrlActions.Trash : +action | +UrlActions.Trash
      );
  }, [active, location.pathname, action]);
}
