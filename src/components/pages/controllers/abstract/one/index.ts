import { useCallback, useEffect, useRef } from 'react';
import { useLocation, useNavigate } from 'react-router';

import { X } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';
import { AsyncAction } from '@avstantso/react--components';

import { ModelStorage } from '@model';

import { InTrash, IsInPathname, Readonly, VisibleActions } from '../utils';

import type { OneEntity as Types } from './types';
import { useActiveTrashRedirect } from './use-active-trash-redirect';

const { useOne } = ModelStorage.Reader;

export namespace OneEntity {
  export namespace Entity {
    export type Raw<TModel extends Model.Structure> = Types.Entity.Raw<TModel>;
  }

  export type Entity<TModel extends Model.Structure> = Types.Entity<TModel>;

  export namespace External {
    export type Common<TModel extends Model.Structure> =
      Types.External.Common<TModel>;

    export type Props<TModel extends Model.Structure> =
      Types.External.Props<TModel>;
  }

  export type Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.ReadableWritable<TModel>
  > = Types.Props<TModel, TModelStorage>;
}

export type OneEntity<TModel extends Model.Structure> = Types<TModel>;

const defaultVisibleActions = UrlActions.fromValue(
  +UrlActions.Trash |
    +UrlActions.View |
    +UrlActions.New |
    +UrlActions.Edit |
    +UrlActions.Delete |
    +UrlActions.Restore
);

export function useOneEntity<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
>(
  props: OneEntity.Props<TModel, TModelStorage> &
    OneEntity.External.Props<TModel>
): OneEntity<TModel> {
  const {
    declaration,
    entity,
    setEntityRaw,
    clacEntity,
    id,
    modelStorage,
    action,
    visibleActions = defaultVisibleActions,
    noOutlet,
  } = props;

  const pathname = `${props.pathname}/${
    null === id ? UrlActions.New.toPath() : id
  }`;
  const backUrl = `${props.pathname}${
    action && +action & +UrlActions.Trash ? `/${UrlActions.Trash.toPath()}` : ''
  }`;

  const readOnly = Readonly(props.readOnly, entity);
  const visible = VisibleActions(action, visibleActions, props.visible);
  const inTrash = InTrash(action);

  const location = useLocation();
  const navigate = useNavigate();

  const isInPathname = IsInPathname(pathname, location);

  const asyncAction = AsyncAction.use();

  const needSyncModel = useRef(false);

  useEffect(() => {
    asyncAction.end();

    if (null === id) setEntityRaw(clacEntity());
  }, [id]);

  useOne<TModel, TModelStorage>(modelStorage, id, {
    setResult: setEntityRaw,
    asyncAction,
  });

  const externalSetEntity = useCallback<OneEntity<TModel>['setEntity']>(
    (param) => {
      needSyncModel.current = true;
      setEntityRaw(param);
    },
    []
  );

  useEffect(() => {
    if (!needSyncModel.current) return;

    needSyncModel.current = false;

    modelStorage.collection.addOrSet(entity);
  }, [entity]);

  useActiveTrashRedirect({
    location,
    navigate,
    entity,
    pathname,
    action,
    label: useOneEntity.name,
  });

  const goBack = () => navigate(backUrl);

  return {
    declaration,
    modelStorage,
    id,
    entity,
    setEntity: externalSetEntity,
    setEntityRaw,
    asyncAction,
    backUrl,
    goBack,
    pathname,
    isInPathname,
    action,
    readOnly,
    visible,
    inTrash,
    noOutlet,
  };
}

function Empty<TModel extends Model.Structure>(): OneEntity<TModel> {
  return {
    declaration: undefined,
    modelStorage: undefined,
    id: undefined,
    entity: undefined,
    setEntity: X,
    setEntityRaw: X,
    asyncAction: AsyncAction.empty,
    backUrl: undefined,
    goBack: X,
    pathname: undefined,
    isInPathname: undefined,
    action: undefined,
    readOnly: undefined,
    visible: undefined,
    inTrash: undefined,
    noOutlet: undefined,
  };
}

export const OneEntity = { Empty, defaultVisibleActions };
