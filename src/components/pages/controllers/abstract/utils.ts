import { Location } from 'react-router';

import { JS, Override, Perform } from '@avstantso/node-or-browser-js--utils';

import { UrlActions } from '@avstantso/react--data';

import { Base } from '../types';

type BaseProps = Base.Controller.Entity.Props<any>;

export function IsInPathname(pathname: string, location: Location): boolean {
  return !pathname || location.pathname.startsWith(pathname);
}

export function Readonly<TParams extends any[] = never>(
  readOnly: Perform<boolean, TParams>,
  ...params: TParams
) {
  return Perform(readOnly, ...params);
}

export function VisibleActions(
  action: UrlActions,
  visibleActions: BaseProps['visibleActions'],
  visible: BaseProps['visible']
): boolean {
  const value =
    undefined === visibleActions ||
    (JS.is.function(visibleActions)
      ? visibleActions(action)
      : +action === (+action & +visibleActions));

  return value && Override.Calculator(value)(visible);
}

export function InTrash(action: UrlActions.Union): boolean {
  return !!(action && +action & +UrlActions.Trash);
}

export function cutPathTail(pathname: string, countToCut = 1) {
  const n = Math.abs(countToCut);

  const parts = pathname.split('/');

  return parts.slice(0, parts.length - n).join('/');
}
