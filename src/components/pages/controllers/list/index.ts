import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { ModelStorage } from '@types';

import type { List as Types } from './types';
import { List as _List } from './controller';

export { useList } from './controller';

export namespace List {
  export namespace Props {
    export namespace DisabledReader {
      export type TypeMap = Types.Props.DisabledReader.TypeMap;
    }

    export type DisabledReader = Types.Props.DisabledReader;
  }

  export type Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.Union<TModel>
  > = Types.Props<TModel, TModelStorage>;

  export type ShowActive = Types.ShowActive;
}

export type List<TModel extends Model.Structure> = Types<TModel>;

export const List = _List;
