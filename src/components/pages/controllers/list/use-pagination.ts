import React, { useEffect, useState } from 'react';
import { Location } from 'react-router';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import { ModelStorage } from '@model';

import type { List } from './types';

export namespace Pagination {
  export type Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.Union<TModel>
  > = {
    location: Location;
  } & Pick<List.Props<TModel, TModelStorage>, 'pathname' | 'pagination'>;

  export type Result = ReturnType<
    typeof useState<ModelStorage.Reader.Pagination>
  >;
}

export function usePagination<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.Union<TModel>
>(props: Pagination.Props<TModel, TModelStorage>): Pagination.Result {
  const { pathname, location } = props;

  const result = useState(props.pagination);
  const [pagination, setPagination] = result;

  useEffect(() => {
    if (pathname && pathname !== location.pathname) return;

    let searchParams: URLSearchParams;
    function addExistsSP(name: string): Model.Pagination {
      if (!searchParams) searchParams = new URLSearchParams(location.search);

      const isNum = 'num' === name;
      const pName = `page${name.toCapitalized()}`;
      const r: any = {};

      if (searchParams.has(pName)) {
        const v =
          Number.parseInt(searchParams.get(pName), 10) - (isNum ? 1 : 0);
        if (!isNaN(v) && ('size' === name ? v > 0 : v >= 0)) r[name] = v;
      }

      return r;
    }

    const { iterable, clear, num, size } = props.pagination?.iterable
      ? props.pagination || ({} as ModelStorage.Reader.Pagination)
      : props.pagination
      ? {
          ...props.pagination,
          ...addExistsSP('num'),
          ...addExistsSP('size'),
        }
      : ({} as ModelStorage.Reader.Pagination);

    if (
      pagination?.iterable !== iterable ||
      pagination?.clear !== clear ||
      pagination?.num !== num ||
      pagination?.size !== size
    )
      setPagination({ iterable, clear, num, size });
  }, [
    props.pagination?.iterable,
    props.pagination?.clear,
    props.pagination?.num,
    props.pagination?.size,
    pathname,
    location.pathname,
    location.search,
  ]);

  return result;
}
