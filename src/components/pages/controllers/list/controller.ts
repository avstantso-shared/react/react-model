import React from 'react';
import { useLocation, useNavigate } from 'react-router';

import { Generics, Override } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';
import { AsyncAction } from '@avstantso/react--components';

import { ModelStorage } from '@model';
import { useHistory } from '@history';

import { InTrash, IsInPathname, Readonly, VisibleActions } from '../abstract';

import type { List as Types } from './types';
import { usePagination } from './use-pagination';
import { useShowActive } from './use-show-active';

const defaultVisibleActions = UrlActions.fromValue(
  +UrlActions.Root | +UrlActions.Trash | +UrlActions.Delete
);

export function useList<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.Union<TModel>
>(props: Types.Props<TModel, TModelStorage>): Types<TModel> {
  const {
    declaration,
    modelStorage,
    source,
    disabledReader,
    search,
    pathname,
    action = UrlActions.Root,
    visibleActions = defaultVisibleActions,
    noOutlet,
  } = props;

  const readOnly = Readonly(props.readOnly);
  const visible = VisibleActions(action, visibleActions, props.visible);
  const inTrash = InTrash(action);

  const location = useLocation();
  const navigate = useNavigate();

  const isInPathname = IsInPathname(pathname, location);

  const { isFrom } = useHistory();
  // AVStantso: do not reload after delete dialog cancellation
  const noReload = isFrom.Delete(
    modelStorage.collection.by(Generics.Cast(Model.Fields.id)).find
  );

  const asyncAction = AsyncAction.use(!noReload);

  const inTrashBinRecs = isInPathname
    ? modelStorage.collection
        .by(Generics.Cast(Model.Fields.active))
        .count(false)
    : 0;

  const [showActive, setShowActive] = useShowActive<TModel, TModelStorage>({
    ...props,
    action,
    isInPathname,
    loading: asyncAction.loading,
    inTrashBinRecs,
    location,
    navigate,
    visible,
    label: useList.name,
  });

  const [pagination] = usePagination<TModel, TModelStorage>({
    ...props,
    location,
  });

  ModelStorage.Reader.useList<TModel, TModelStorage>(modelStorage, {
    asyncAction,
    noReload: Override.Calculator(noReload)(props.noReload),
    source,
    disabled: Override.Calculator({
      original: !isInPathname,
      location,
    })(disabledReader),
    pagination,
    search,
  });

  return {
    declaration,
    modelStorage,
    asyncAction,
    showActive,
    setShowActive,
    pagination,
    pathname,
    isInPathname,
    action,
    readOnly,
    inTrashBinRecs,
    visible,
    inTrash,
    noOutlet,
  };
}

export const List = { defaultVisibleActions };
