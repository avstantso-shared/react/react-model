import type { Location } from 'react-router';

import type { Override, Perform } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { StateRec } from '@avstantso/react--data';

import type { ModelStorage } from '@types';

import type { Base } from '../types';

export namespace List {
  export namespace Props {
    export namespace DisabledReader {
      export type TypeMap = { T: boolean; Ext: { location: Location } };
    }

    export type DisabledReader = Override.Union<DisabledReader.TypeMap>;
  }

  export interface Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.Union<TModel>
  > extends ModelStorage.Union.Provider<TModel, TModelStorage>,
      Base.Controller.Entity.Props<TModel>,
      Model.Select.Options<TModel, ModelStorage.Reader.Pagination> {
    noReload?: Override<boolean>;
    source?: TModel['Select'][];
    disabledReader?: Props.DisabledReader;
    readOnly?: Perform<boolean>;
  }

  export type ShowActive = StateRec<'showActive', boolean>;
}

export interface List<TModel extends Model.Structure>
  extends Base.Controller<TModel>,
    List.ShowActive {
  pagination?: ModelStorage.Reader.Pagination;
  inTrashBinRecs: number;
}
