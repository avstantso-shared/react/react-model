import React, { useEffect, useState } from 'react';
import { NavigateFunction, Location } from 'react-router';

import { Perform } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions, AsyncAction } from '@avstantso/react--data';

import { ModelStorage } from '@model';

import type { List } from './types';

export namespace ShowActive {
  export type Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.Union<TModel>
  > = {
    location: Location;
    navigate: NavigateFunction;
    label: string;
  } & Pick<
    List.Props<TModel, TModelStorage>,
    'modelStorage' | 'pathname' | 'action'
  > &
    Pick<List<any>, 'isInPathname' | 'inTrashBinRecs' | 'visible'> &
    Pick<AsyncAction, 'loading'>;

  export type Result = ReturnType<typeof useState<boolean>>;
}

export function useShowActive<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.Union<TModel>
>(props: ShowActive.Props<TModel, TModelStorage>): ShowActive.Result {
  const {
    modelStorage,
    pathname,
    action,
    isInPathname,
    loading,
    inTrashBinRecs,
    location,
    navigate,
    label,
    visible,
  } = props;

  const showActive: ShowActive.Result[0] = !(+action & +UrlActions.Trash);

  function setShowActiveRaw(newShowActive: boolean, replace?: boolean): void {
    if (!pathname)
      throw new InvalidArgumentError(
        `${modelStorage.name} list controller must have "pathname" prop for use "setShowActive"`
      );

    const toPathname = UrlActions.replaceInPath(
      location.pathname,
      pathname,
      newShowActive ? +action & ~UrlActions.Trash : +action | +UrlActions.Trash
    );
    navigate(toPathname, {
      state: {
        location,
        label: `Pages.Controllers.${label}${useShowActive.name}`,
      },
      ...(replace ? { replace } : {}),
    });
  }

  const setShowActive: ShowActive.Result[1] = (value) =>
    setShowActiveRaw(Perform(value, showActive));

  const needCloseTrashBin =
    isInPathname &&
    modelStorage.isReady &&
    visible &&
    !loading &&
    !showActive &&
    !inTrashBinRecs;

  useEffect(() => {
    if (needCloseTrashBin) setShowActiveRaw(true, true);
  }, [needCloseTrashBin]);

  return [showActive, setShowActive];
}
