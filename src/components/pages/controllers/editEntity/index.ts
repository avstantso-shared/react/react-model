import { useCallback } from 'react';
import { useNavigate } from 'react-router';
import axios from 'axios';

import { Generics, X } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';
import {
  Dialog,
  EntityLocalStorage,
  ErrorBox,
} from '@avstantso/react--components';

import type { EntityName, ModelStorage } from '@types';
import { useToasts } from '@toasts';

import { OneEntity, useOneEntity, VisibleActions } from '../abstract';

export namespace EditEntity {
  /**
   * @summary `Model.Structure` to `Dialog.Entity.Controller.TypeMap`
   */
  export type ToDECTM<
    TModel extends Model.Structure,
    TTypeMap extends Dialog.Entity.Controller.TypeMap<
      TModel['Select'],
      TModel['Insert']
    > = Dialog.Entity.Controller.TypeMap<TModel['Select'], TModel['Insert']>
  > = TTypeMap;

  export namespace Complex {
    export namespace Settings {
      export interface Item {
        caption: string | JSX.Element;
        value: string | JSX.Element;
      }

      export type EditHandler = () => void;
    }

    export interface Props<TModel extends Model.Structure> {
      readonly settings?: JSX.Element | string | ReadonlyArray<Settings.Item>;
      settingsEdit?: Settings.EditHandler;
    }
  }

  export interface Props<
    TModel extends Model.Structure,
    TModelStorage extends ModelStorage.ReadableWritable<TModel>
  > extends OneEntity.Props<TModel, TModelStorage>,
      Complex.Props<TModel> {
    entityName?: EntityName.Union;
  }

  export type Options<TModel extends Model.Structure> = Omit<
    Dialog.Entity.Controller.Options<ToDECTM<TModel>>,
    'storage'
  >;
}

export type EditEntity<TModel extends Model.Structure> =
  Dialog.Entity.Controller<EditEntity.ToDECTM<TModel>> &
    OneEntity<TModel> &
    EditEntity.Complex.Props<TModel> & {
      isInsert: boolean;
      entityName: EntityName.Union;
    };

export function useEditEntity<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
>(
  props: EditEntity.Props<TModel, TModelStorage>,
  options?: EditEntity.Options<TModel>
): EditEntity<TModel> {
  //#region types
  type DECOptions = Dialog.Entity.Controller.Options<
    EditEntity.ToDECTM<TModel>
  >;
  //#endregion

  const {
    declaration,
    modelStorage,
    entityName,
    id,
    settings,
    settingsEdit,
    pathname,
    action,
    visibleActions = OneEntity.defaultVisibleActions, // Copy from OneEntity
  } = props;

  const isInsert = !id;
  const { canSubmit: canSubmitExternal } = options || {};
  const visible = VisibleActions(action, visibleActions, props.visible); // Copy from OneEntity

  const navigate = useNavigate();
  const { entityIdIsActioned } = useToasts();

  const findOriginalEntity = useCallback(() => {
    const r =
      (modelStorage.isReady &&
        modelStorage.collection.by(Generics.Cast(Model.Fields.id)).find(id)) ||
      undefined;

    return r;
  }, [modelStorage.isReady && modelStorage.collection, id]);

  // #region Declaration: functions resolve dependencies for OneController
  let goBack: () => void;
  let doSubmit: DECOptions['doSubmit'];
  let catchSubmit: DECOptions['catchSubmit'];
  // #endregion

  // A.V.Stantso:
  // For a entity control uses DialogController inside
  const dlgController = Dialog.Entity.useController<EditEntity.ToDECTM<TModel>>(
    {
      entity: findOriginalEntity,
      handler: Dialog.Data.handleDlg<any>(
        async (result) => {
          if (result.isClosed) result.close();
        },
        () => goBack()
      ),
    },
    {
      ...Generics.Cast(options),

      ...(canSubmitExternal ? { canSubmit: canSubmitExternal } : {}),
      doSubmit: async (values) => doSubmit(values),
      catchSubmit: async (error, values) => catchSubmit(error, values),

      storage: undefined, // builtin localData don't used
    }
  );
  const { entity, setEntityRaw, clacEntity, formik } = dlgController;

  const oneController = useOneEntity<TModel, TModelStorage>({
    ...props,
    entity,
    setEntityRaw,
    clacEntity,
    id,
  });
  const { asyncAction, backUrl } = oneController;

  // #region Implementation: functions resolve dependencies for OneController
  goBack = oneController.goBack;

  doSubmit = async (values) => {
    return Promise.resolve(
      id ? modelStorage.update(values) : modelStorage.insert(values)
    ).then((fromServer) => {
      setEntityRaw(fromServer);

      entityIdIsActioned(
        fromServer.id,
        isInsert ? UrlActions.New : UrlActions.Edit,
        entityName
      );

      if (isInsert)
        navigate(UrlActions.Edit.toPathPostfix(backUrl, fromServer.id));

      localData.clear.submitted();

      return fromServer;
    });
  };

  catchSubmit = asyncAction.setLastError;
  // #endregion

  // Copy from Dialog.Entity.useController
  // need OneController.asyncAction.loading
  type ELS = Dialog.Entity.Controller.TypeMap.ToELS<EditEntity.ToDECTM<TModel>>;
  const localData = EntityLocalStorage.SyncFormik.use<ELS>({
    disabled: !modelStorage.isReady || !visible || asyncAction.loading,
    storageKey: () => UrlActions.toPathPostfix(action, pathname, id),
    schema: declaration.Schema.Insert,
    formik,
    clacEntity,
  });

  return {
    ...dlgController,
    ...oneController,
    settings,
    settingsEdit,
    isInsert,
    entityName,
    localData,
  };
}

function Empty<TModel extends Model.Structure>(): EditEntity<TModel> {
  return {
    ...Dialog.Entity.useController.Empty<EditEntity.ToDECTM<TModel>>(),
    ...OneEntity.Empty<TModel>(),

    settings: undefined,
    settingsEdit: X,
    isInsert: undefined,
    entityName: undefined,
    localData: undefined,
  };
}
export const EditEntity = {
  Empty,
};
