import type { Override } from '@avstantso/node-or-browser-js--utils';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { UrlActions } from '@avstantso/react--data';
import type { AsyncAction } from '@avstantso/react--components';

export namespace Base {
  export namespace Controller {
    export namespace Entity {
      export interface Props<TModel extends Model.Structure> {
        readonly declaration: Model.Declaration<TModel>;
        readonly id?: Model.ID;
        readonly pathname?: string;
        readonly action?: UrlActions;
        readonly?: unknown;
        visibleActions?: UrlActions.Union | ((action?: UrlActions) => boolean);
        visible?: Override.Union<boolean>;
        noOutlet?: boolean;
      }
    }

    export interface Entity<TModel extends Model.Structure>
      extends Controller<TModel> {
      readonly id: Model.ID;
    }
  }

  export type Controller<TModel extends Model.Structure> = Readonly<
    AsyncAction.Readonly.Provider & {
      declaration: Model.Declaration<TModel>;
      modelStorage: unknown;
      pathname: string;
      isInPathname: boolean;
      action: UrlActions;
      readOnly: boolean;
      visible: boolean;
      inTrash: boolean;
      noOutlet: boolean;
    }
  >;
}
