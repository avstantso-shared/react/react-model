import { Model } from '@avstantso/node-or-browser-js--model-core';

import { AlternateJSX } from '@avstantso/react--data';

import { FormControl } from '@types';

type GetCaption = <TModel extends Model.Structure>(
  raw: AlternateJSX.Getter.Raw<[TModel['Select']]>
) => FormControl.GetCaption<TModel>;

export const GetCaption: GetCaption = AlternateJSX.Getter;
