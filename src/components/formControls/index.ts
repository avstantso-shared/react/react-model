import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { ModelStorage, FormControl as Types } from '@types';

import * as _ from './internals';

export namespace FormControls {
  export type GetCaption<TModel extends Model.Structure> =
    Types.GetCaption<TModel>;
  export type FindByValue<TModel extends Model.Structure> =
    Types.FindByValue<TModel>;

  export namespace EntityDropdown {
    export namespace Props {
      export type Visual = Types.EntityDropdown.Props.Visual;
    }

    export type Props<
      TModel extends Model.Structure,
      TModelStorage extends ModelStorage.ReadableWritable<TModel>
    > = Types.EntityDropdown.Props<TModel, TModelStorage>;
  }
}

export const FormControls = { ..._ };
