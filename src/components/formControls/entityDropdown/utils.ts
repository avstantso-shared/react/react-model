import { Model } from '@avstantso/node-or-browser-js--model-core';

import { type FormControl } from '@types';

export const defaultGetCaption: FormControl.GetCaption<any> = (item) =>
  `${Model.Named.Force(item)?.name || item?.id}`;

export function DefaultFindByValue<TModel extends Model.Structure>(
  filtered: ReadonlyArray<TModel['Select']>
): FormControl.FindByValue<TModel> {
  return (text) => filtered.find(Model.IDed.Predicate({ id: text }));
}
