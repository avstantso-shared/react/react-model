import React, { useMemo } from 'react';
import { Dropdown, DropdownButton, Form, InputGroup } from 'react-bootstrap';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import { useAsyncAction, Loading } from '@avstantso/react--components';

import { FormControl } from '@types';
import { ModelStorage } from '@model';

import { DefaultFindByValue, defaultGetCaption } from './utils';

const { useList } = ModelStorage.Reader;

export function EntityDropdown<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
>(props: FormControl.EntityDropdown.Props<TModel, TModelStorage>) {
  const {
    modelStorage,
    id: elementId,
    variant,
    dropdownAlign,
    getCaption: caption = defaultGetCaption,
    findByValue,
    value,
    setValue,
    onEntityChange,
    filter,
    leftChildren,
    rightChildren,
    feedback,
    ...formCtrlProps
  } = props;

  const asyncAction = useAsyncAction();
  const { loading, Boundary } = asyncAction;

  const filtered = useMemo(
    () =>
      filter
        ? modelStorage.collection.list.filter(filter)
        : modelStorage.collection.list,
    [modelStorage.collection.list.length]
  );

  const find = findByValue || DefaultFindByValue(filtered);

  function changeEntity(item: TModel['Select'] = null) {
    setValue && setValue(item ? caption(item, true) : '');
    onEntityChange && onEntityChange(item);
  }

  const handleTextChange: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    const vl = event.target.value.trim();
    if (vl === value) return;

    if (!vl) {
      changeEntity();
      return;
    }

    const item = find(vl);
    item ? changeEntity(item) : setValue && setValue(vl);
  };

  useList<TModel, TModelStorage>(modelStorage, {
    asyncAction,
    late: true,
  });

  return (
    <>
      <InputGroup className="mb-3">
        {leftChildren}
        <Form.Control
          {...formCtrlProps}
          value={value || ''}
          id={elementId}
          aria-label="Entity dropdown"
          onChange={handleTextChange}
          list="variants"
          autoComplete="off"
        />
        {loading ? (
          <InputGroup.Text>
            <Loading sm />
          </InputGroup.Text>
        ) : (
          <DropdownButton
            variant={variant}
            title=""
            id={elementId && `${elementId}-dropdown`}
            align={dropdownAlign || 'end'}
            disabled={!filtered.length || formCtrlProps.readOnly}
          >
            {filtered.map((item) => (
              <Dropdown.Item
                key={item.id}
                onClick={changeEntity.bind(null, item)}
              >
                {caption(item)}
              </Dropdown.Item>
            ))}
          </DropdownButton>
        )}
        {rightChildren}
        <datalist id="variants">
          {filtered.map((item) => (
            <option key={item.id} value={caption(item, true)} />
          ))}
        </datalist>
        <Form.Control.Feedback type="invalid">{feedback}</Form.Control.Feedback>
      </InputGroup>
      <Boundary.ErrorBox />
    </>
  );
}
