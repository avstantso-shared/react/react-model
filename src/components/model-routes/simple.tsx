import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

import { Types, ModelRoute } from './abstract';

export namespace Simple {
  export type SubRoutes<TSubRoute extends Types.SubRoute> =
    Types.SubRoutes<TSubRoute>;

  export interface Structure<TModel extends Model.Structure>
    extends Types.Structure<TModel> {
    SubRoutes?: SubRoutes<Structure<TModel>['SubRoute']>;
  }

  export type Context<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>
  > = Types.Context<TModel, TRoutes>;

  export namespace Routes {
    export type Props<
      TModel extends Model.Structure,
      TRoutes extends Structure<TModel>
    > = Types.Routes.Props<TModel, TRoutes, Context<TModel, TRoutes>>;
  }
}

export function Simple<
  TModel extends Model.Structure,
  TRoutes extends Simple.Structure<TModel> = Simple.Structure<TModel>
>() {
  return ModelRoute<
    TModel,
    TRoutes,
    Simple.Context<TModel, TRoutes>,
    Simple.Routes.Props<TModel, TRoutes>
  >();
}
