import { Outlet } from 'react-router';

import { JS, Perform } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';
import { FCCache, RouteObject, useEBRoute } from '@avstantso/react--components';

import type { Options, Structure, SubRoute } from './types';

export namespace MakeRoute {
  export type Props<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>
  > = { wrapRoute: SubRoute.Wrap<TRoutes['SubRoute']> } & Pick<
    Options<TModel, TRoutes, any>,
    'topViews'
  >;
}

export type MakeRoute<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel> = Structure<TModel>
> = {
  (
    kind: UrlActions,
    subRouteOrFC: TRoutes['SubRoute'] | FCCache.FC
  ): RouteObject;
} & Record<
  UrlActions.TypeMap['Key'],
  (subRouteOrFC: TRoutes['SubRoute'] | FCCache.FC) => RouteObject
>;

export function MakeRoute<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel> = Structure<TModel>
>({
  wrapRoute,
  topViews,
}: MakeRoute.Props<TModel, TRoutes>): MakeRoute<TModel, TRoutes> {
  function f(kind: UrlActions, subRouteOrFC: TRoutes['SubRoute'] | FCCache.FC) {
    const path = UrlActions.toPath(kind);

    const subRoute: TRoutes['SubRoute'] = JS.is.object<TRoutes['SubRoute']>(
      subRouteOrFC
    )
      ? subRouteOrFC
      : { FC: subRouteOrFC };

    const route: RouteObject = (() => {
      if (null === subRoute) return { path, element: null };

      const { FC, props, sitemapMeta } = subRoute;
      const children = Perform(subRoute.children);

      return {
        path,
        ...(+kind & +topViews
          ? { element: <Outlet /> }
          : FC
          ? { element: <FC {...props} /> }
          : {}),
        ...(children
          ? { children: Array.isArray(children) ? children : [children] }
          : {}),
        ...(sitemapMeta ? { sitemapMeta } : {}),
      };
    })();

    return wrapRoute ? wrapRoute({ kind, subRoute, route }) : useEBRoute(route);
  }

  return UrlActions._extender(
    f,
    (kind, f) => (subRouteOrFC: TRoutes['SubRoute'] | FCCache.FC) =>
      f(kind, subRouteOrFC)
  );
}
