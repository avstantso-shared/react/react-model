import { Outlet } from 'react-router';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';
import { FCCache, RouteObject } from '@avstantso/react--components';

import { Structure, Context, Routes, Options } from './types';

import { MakeRoute } from './make-route';
import * as Layouts from './layouts';

export function makeRouteInternals<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>
>(
  Context: React.Context<TContext>,
  props: Routes.Props<TModel, TRoutes, TContext>,
  options: Options<TModel, TRoutes, TContext>
): RouteObject {
  type Source = TRoutes['SubRoute'] | FCCache.FC;

  const {
    path,
    GlobalOutlet,
    wrapRoute,
    allowView,
    subRoutes,
    paramIdName = 'id',
    deleteProps,
  } = props;

  const { rootExt, topViews } = { ...Options.Default, ...options };

  const makeRoute = MakeRoute<TModel, TRoutes>({ wrapRoute, topViews });

  function makeTopViewsRoute(
    parent: RouteObject,
    children: RouteObject[]
  ): RouteObject {
    const sources: Source[] = [];
    let current = parent;

    [
      { a: UrlActions.Root, s: subRoutes.root },
      { a: UrlActions.Edit, s: subRoutes.edit },
      { a: UrlActions.View, s: subRoutes.view || subRoutes.edit },
      { a: UrlActions.New, s: subRoutes.new || subRoutes.edit },
    ].forEach(({ a: action, s: source }) => {
      if (!(+action & +topViews) || !source || sources.includes(source)) return;

      sources.push(source);

      const next: RouteObject = {
        path: '',
        element: <Layouts.WrapView {...{ action, source, allowView }} />,
        children: [],
      };

      current.children.push(next);

      current = next;
    });

    current.children.unshift(...children);

    return parent;
  }

  function makeRestoreRoute(): RouteObject {
    return makeRoute.Restore(
      FCCache.Value(Layouts.Restore<TModel, TRoutes, TContext>, {
        Context,
        path,
      })
    );
  }

  function makeDeleteRoute(): RouteObject {
    return makeRoute.Delete(
      FCCache.Value(Layouts.Delete<TModel, TRoutes, TContext>, {
        Context,
        path,
        deleteProps,
      })
    );
  }
  makeDeleteRoute.inArr = () => (deleteProps ? [makeDeleteRoute()] : []);

  function makeParamIdRoute(editable?: boolean): RouteObject {
    return {
      path: `:${paramIdName}`,
      element: <Outlet />,
      children: [
        makeRoute.View(subRoutes.view || subRoutes.edit),
        editable ? makeRoute.Edit(subRoutes.edit) : makeRestoreRoute(),
        ...makeDeleteRoute.inArr(),
      ],
    };
  }

  function makeTrashRoute(): RouteObject {
    const {
      path,
      sitemapMeta,
      children = [],
    } = makeRoute.Trash(subRoutes.root);

    return {
      path,
      element: <Outlet />,
      sitemapMeta,
      children: [
        { path: '', element: null },
        makeParamIdRoute(false),
        ...children,
      ],
    };
  }

  function makeRootRoute(): RouteObject {
    const {
      path,
      element,
      sitemapMeta,
      children = [],
    } = makeRoute.Root(subRoutes.root);

    return {
      path,
      element,
      sitemapMeta,
      children: [
        makeTrashRoute(),
        makeRoute.New(subRoutes.new || subRoutes.edit),
        makeParamIdRoute(true),
        ...children,
      ],
    };
  }

  const internal = makeTopViewsRoute(
    {
      element: <Layouts.Root {...{ Context, ...props, ...rootExt }} />,
      children: [],
    },
    [makeRootRoute()]
  );

  return {
    path,
    ...(GlobalOutlet
      ? { element: <GlobalOutlet />, children: [{ path: '', ...internal }] }
      : internal),
  };
}
