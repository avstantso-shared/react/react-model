import { ErrorBox } from '@avstantso/react--components';

import { UnableLoadError } from '@classes';

import { IdError } from '../../types';

export const DefaultIdError: IdError.FC = (props) => (
  <ErrorBox error={UnableLoadError.invalidId(props.id)} data={props} />
);
