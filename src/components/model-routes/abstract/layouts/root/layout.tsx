import { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Structure, Context, Root } from '../../types';

import { useDetectIdAction } from './hooks';
import { DefaultIdError } from './default-id-error';
import { RootValidLayout } from './valid-layout';

export function RootLayout<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>
>(props: Root.Props<TModel, TRoutes, TContext>): JSX.Element {
  const {
    Context,
    IdErrorFC,
    declaration,
    path,
    paramIdName,
    subRoutes,
    controllers,
    extendContext,
  } = props;

  const { action, id, hasIdError } = useDetectIdAction(path, paramIdName);

  return hasIdError ? (
    !IdErrorFC ? (
      <DefaultIdError id={id} />
    ) : (
      <IdErrorFC id={id}>
        <DefaultIdError id={id} />
      </IdErrorFC>
    )
  ) : (
    <RootValidLayout
      {...{
        Context,
        declaration,
        path,
        subRoutes,
        controllers,
        extendContext,
        action,
        id,
      }}
    />
  );
}
