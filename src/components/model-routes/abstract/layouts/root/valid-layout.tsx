import { Outlet } from 'react-router';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Structure, Context, Root, Controllers } from '../../types';

import { useActionWithHistory } from './hooks';

export function RootValidLayout<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>
>(props: Root.Valid.Props<TModel, TRoutes, TContext>): JSX.Element {
  const {
    Context,
    declaration,
    controllers: makeControllers,
    id,
    extendContext,
  } = props;

  const action = useActionWithHistory(props);

  const ext = extendContext && extendContext();

  const controllers: Controllers<TModel, TRoutes> = {
    root: makeControllers.root({ declaration, action, ...ext }),
    edit: makeControllers.edit({ declaration, id, action, ...ext }),
  };

  const rawContext: Context<TModel, TRoutes> = {
    controllers,
    modelStorage: controllers.root.modelStorage,
    action,
    ...(id && { id }),
  };

  const context: TContext = Generics.Cast.To({ ...rawContext, ...ext });

  return (
    <Context.Provider value={context}>
      <Outlet />
    </Context.Provider>
  );
}
