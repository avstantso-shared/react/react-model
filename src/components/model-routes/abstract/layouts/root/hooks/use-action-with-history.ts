import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

import { useHistory } from '@history';

import type { Structure, Context, Root } from '../../../types';

export function useActionWithHistory<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>
>(
  props: Pick<
    Root.Valid.Props<TModel, TRoutes, TContext>,
    'path' | 'action' | 'id'
  >
): UrlActions {
  const { path, id } = props;

  const { from } = useHistory();

  if (+props.action & (+UrlActions.Delete | +UrlActions.Restore)) {
    const prev = from().get(path);

    if (prev && id === prev.id)
      return UrlActions.fromValue(
        +props.action | (+prev.action & (+UrlActions.View | +UrlActions.Edit))
      );
  }

  return props.action;
}
