import { validate as isValidUUID } from 'uuid';
import { useLocation } from 'react-router';

import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

function useGetPathParts(path: string): UrlActions.PathParts {
  const location = useLocation();

  const allParts = location.pathname.split('/');
  const pathIndex = allParts.indexOf(path);
  if (pathIndex < 0)
    throw new InvalidArgumentError(
      `Path "${path}" not found in pathname "${location.pathname}"`
    );

  return allParts
    .splice(pathIndex + 1, UrlActions.PathParts.max)
    .pack() as UrlActions.PathParts;
}

export function useDetectIdAction(path: string, paramIdName: string) {
  const parts = useGetPathParts(path);

  const action = UrlActions.fromPathParts(parts);

  const isRoot = UrlActions.is.Root(action);
  const isTrash = !isRoot && !!(+UrlActions.Trash & +action);
  const isTrashRoot = isTrash && 1 === parts.length;
  const isNew = !isRoot && !isTrash && UrlActions.is.New(action);

  const rawId = action.idFromPathParts(parts);

  const hasIdError = !(isRoot || isNew || isTrashRoot || isValidUUID(rawId));

  const id: Model.ID = isRoot || isTrashRoot ? undefined : isNew ? null : rawId;

  return { action, id, hasIdError };
}
