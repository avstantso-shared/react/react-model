import React from 'react';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import * as Pages from '@components/pages';

import type { Structure, Context } from '../types';

export function RestoreLayout<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>
>(props: Context.Provider<TModel, TRoutes, TContext>) {
  const { Context, ...rest } = props;

  const {
    modelStorage,
    action,
    id,
    controllers: {
      edit: { entityName },
    },
  } = React.useContext(Context);

  return (
    <Pages.RestoreEntity
      modelStorage={Generics.Cast.To(modelStorage)}
      entity={id}
      {...{ entityName, action, ...rest }}
    />
  );
}
