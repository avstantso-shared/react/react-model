import React from 'react';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import * as Pages from '@components/pages';

import type { Structure, Context, Routes } from '../types';

export function DeleteLayout<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>,
  Props extends Routes.Props<TModel, TRoutes, TContext> = Routes.Props<
    TModel,
    TRoutes,
    TContext
  >
>(
  props: Pick<Props, 'deleteProps'> &
    Context.Provider<TModel, TRoutes, TContext>
) {
  const { Context, deleteProps } = props;

  const {
    modelStorage,
    id,
    controllers: {
      edit: { entityName },
    },
  } = React.useContext(Context);

  return (
    <Pages.DeleteEntity
      modelStorage={Generics.Cast.To(modelStorage)}
      entity={id}
      {...{ entityName, ...deleteProps }}
    />
  );
}
