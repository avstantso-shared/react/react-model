import { Outlet } from 'react-router';

import { UrlActions } from '@avstantso/react--data';
import { FCCache } from '@avstantso/react--components';

import { SubRoute } from '../types';

export namespace WrapViewLayout {
  export type Props<TSubRoute extends SubRoute> = {
    action: UrlActions;
    source: SubRoute | FCCache.FC;
    allowView: SubRoute.AllowView<TSubRoute>;
  };
}

export function WrapViewLayout<TSubRoute extends SubRoute>({
  action: kind,
  source,
  allowView,
}: WrapViewLayout.Props<TSubRoute>): JSX.Element {
  const subRoute: any = source;

  const allow = !allowView || allowView({ kind, subRoute });

  if (!allow) return <Outlet />;

  const { FC, props } = FCCache.Value(source);

  return <FC {...props} />;
}
