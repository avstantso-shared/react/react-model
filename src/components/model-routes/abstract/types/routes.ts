import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { SubRoute } from './sub-routes';
import type { Structure } from './structure';
import type { Context, Controllers } from './context-controllers';
import type { IdError } from './id-error';

export namespace Routes {
  export interface Props<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>,
    TContext extends Context<TModel, TRoutes>
  > extends Controllers.Make.Provider<TModel, TRoutes, TContext>,
      IdError.FC.Provider {
    declaration: Model.Declaration<TModel>;
    path: string;
    paramIdName?: string;
    subRoutes: TRoutes['SubRoutes'];
    deleteProps?: TRoutes['DeleteProps'];
    wrapRoute?: SubRoute.Wrap<TRoutes['SubRoute']>;
    allowView?: SubRoute.AllowView<TRoutes['SubRoute']>;
    GlobalOutlet?: React.FC;
  }
}
