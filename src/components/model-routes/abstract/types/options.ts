import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

import type { Structure } from './structure';
import type { Context } from './context-controllers';
import type { Root } from './root';
import type { Routes } from './routes';

export namespace Options {
  export type Get<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>,
    TContext extends Context<TModel, TRoutes>,
    TProps extends Routes.Props<TModel, TRoutes, TContext>
  > = (props: TProps) => Options<TModel, TRoutes, TContext>;
}

export type Options<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>
> = {
  rootExt?: Root.Ext.Props<TModel, TRoutes, TContext>;

  /**
   * @default +UrlActions.Root | +UrlActions.View | +UrlActions.New | +UrlActions.Edit
   */
  topViews?: UrlActions.Union;
};

export const Options = {
  Default: {
    topViews:
      +UrlActions.Root | +UrlActions.View | +UrlActions.New | +UrlActions.Edit,
  },
};
