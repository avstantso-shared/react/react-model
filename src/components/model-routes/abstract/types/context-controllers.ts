import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

import type * as Pages from '@components/pages';

import type { Structure } from './structure';

export type Constraint<TModel extends Model.Structure> = Pick<
  Structure<TModel>,
  'Edit' | 'Root'
>;

export namespace Controllers {
  export interface Provider<
    TModel extends Model.Structure,
    TRoutes extends Constraint<TModel>
  > {
    readonly controllers: Readonly<Controllers<TModel, TRoutes>>;
  }
}

export type Controllers<
  TModel extends Model.Structure,
  TRoutes extends Constraint<TModel>
> = {
  root: TRoutes['Root'];
  edit: TRoutes['Edit'];
};

export namespace Context {
  export type Diff<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>,
    TContext extends Context<TModel, TRoutes>
  > = Omit<TContext, keyof Context<TModel, Structure<TModel>>>;

  export type Provider<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>,
    TContext extends Context<TModel, TRoutes>
  > = { Context: React.Context<TContext> };
}

export type Context<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>
> = Controllers.Provider<TModel, TRoutes> &
  Pick<Controllers<TModel, TRoutes>['root'], 'modelStorage'> & {
    readonly action: UrlActions;
    readonly id?: Model.ID;
  };

export namespace Controllers {
  export namespace Make {
    export namespace Root {
      export type Props<
        TModel extends Model.Structure,
        TRoutes extends Constraint<TModel>,
        TContext extends Context<TModel, TRoutes>
      > = Context.Diff<TModel, TRoutes, TContext> &
        Pick<Pages.Controllers.List<TModel>, 'declaration' | 'action'>;
    }

    export type Root<
      TModel extends Model.Structure,
      TRoutes extends Constraint<TModel>,
      TContext extends Context<TModel, TRoutes>
    > = (props?: Root.Props<TModel, TRoutes, TContext>) => TRoutes['Root'];

    export namespace Edit {
      export type Props<
        TModel extends Model.Structure,
        TRoutes extends Constraint<TModel>,
        TContext extends Context<TModel, TRoutes>
      > = Context.Diff<TModel, TRoutes, TContext> &
        Pick<
          Pages.Controllers.EditEntity<TModel>,
          'declaration' | 'id' | 'action'
        >;
    }

    export type Edit<
      TModel extends Model.Structure,
      TRoutes extends Constraint<TModel>,
      TContext extends Context<TModel, TRoutes>
    > = (props?: Edit.Props<TModel, TRoutes, TContext>) => TRoutes['Edit'];

    export type Provider<
      TModel extends Model.Structure,
      TRoutes extends Constraint<TModel>,
      TContext extends Context<TModel, TRoutes>
    > = {
      readonly controllers: Make<TModel, TRoutes, TContext>;
    };
  }

  export type Make<
    TModel extends Model.Structure,
    TRoutes extends Constraint<TModel>,
    TContext extends Context<TModel, TRoutes>
  > = {
    root: Make.Root<TModel, TRoutes, TContext>;
    edit: Make.Edit<TModel, TRoutes, TContext>;
  };
}
