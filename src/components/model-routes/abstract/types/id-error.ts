import type React from 'react';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

export namespace IdError {
  export type Props = { id?: Model.ID };

  export type FC = React.FC<Props>;

  export namespace FC {
    export type Provider = {
      IdErrorFC?: FC;
    };
  }
}
