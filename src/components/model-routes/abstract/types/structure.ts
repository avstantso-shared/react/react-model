import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { ModelStorage } from '@model';
import type * as Pages from '@components/pages';

import type { SubRoute, SubRoutes } from './sub-routes';

/**
 * @summary ModelRoutes structure
 *
 * All subtypes fields is optional for setup in derived code
 */
export interface Structure<TModel extends Model.Structure> {
  SubRoute?: SubRoute;
  SubRoutes?: SubRoutes<Structure<TModel>['SubRoute']>;
  Root?: Pages.Controllers.List<TModel>;
  Edit?: Pages.Controllers.EditEntity<TModel>;

  DeleteProps?: Omit<
    Pages.DeleteEntity.Props<TModel, ModelStorage.ReadableWritable<TModel>>,
    'modelStorage' | 'entity' | 'entityName'
  >;
}
