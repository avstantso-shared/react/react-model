import type React from 'react';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Structure } from './structure';
import type { Context as AbstractContext } from './context-controllers';
import type { Routes } from './routes';

export namespace Root {
  export namespace Ext {
    export namespace Children {
      export type Context<
        TModel extends Model.Structure,
        TRoutes extends Structure<TModel>,
        TContext extends AbstractContext<TModel, TRoutes>
      > = TContext;
    }

    export type FC<
      TModel extends Model.Structure,
      TRoutes extends Structure<TModel>,
      TContext extends AbstractContext<TModel, TRoutes>
    > = React.FC<AbstractContext.Diff<TModel, TRoutes, TContext>>;

    export type Props<
      TModel extends Model.Structure,
      TRoutes extends Structure<TModel>,
      TContext extends AbstractContext<TModel, TRoutes>
    > = {
      extendContext?: () => AbstractContext.Diff<TModel, TRoutes, TContext>;
    };
  }

  export namespace Valid {
    export type Props<
      TModel extends Model.Structure,
      TRoutes extends Structure<TModel>,
      TContext extends AbstractContext<TModel, TRoutes>
    > = Pick<
      Routes.Props<TModel, TRoutes, TContext>,
      'declaration' | 'path' | 'controllers'
    > &
      Pick<TContext, 'action' | 'id'> &
      Ext.Props<TModel, TRoutes, TContext> & {
        Context: React.Context<TContext>;
      };
  }

  export type Props<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>,
    TContext extends AbstractContext<TModel, TRoutes>
  > = Routes.Props<TModel, TRoutes, TContext> &
    Ext.Props<TModel, TRoutes, TContext> & {
      Context: React.Context<TContext>;
    };
}
