import type { Perform } from '@avstantso/node-or-browser-js--utils';

import type { UrlActions } from '@avstantso/react--data';
import type { FCCache, RouteObject } from '@avstantso/react--components';

export interface SubRoute
  extends Pick<RouteObject, 'sitemapMeta'>,
    FCCache.Value {
  children?: Perform<RouteObject[] | RouteObject>;
}

export namespace SubRoute {
  export interface Props<TSubRoute extends SubRoute> {
    readonly kind?: UrlActions;
    readonly subRoute: TSubRoute;
  }

  export type MakeElement<TSubRoute extends SubRoute> = (
    props: Props<TSubRoute>
  ) => React.ReactElement;

  export namespace Wrap {
    export interface Props<TSubRoute extends SubRoute>
      extends SubRoute.Props<TSubRoute> {
      route: RouteObject;
    }
  }

  export type Wrap<TSubRoute extends SubRoute> = (
    props: Wrap.Props<TSubRoute>
  ) => RouteObject;

  export namespace AllowView {
    export type Props<TSubRoute extends SubRoute> = SubRoute.Props<TSubRoute>;
  }

  export type AllowView<TSubRoute extends SubRoute> = (
    props: AllowView.Props<TSubRoute>
  ) => boolean;
}

export interface SubRoutes<TSubRoute extends SubRoute> {
  root: TSubRoute | FCCache.FC;
  edit: TSubRoute | FCCache.FC;
  view?: TSubRoute | FCCache.FC;
  new?: TSubRoute | FCCache.FC;
}
