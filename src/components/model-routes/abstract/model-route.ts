import React from 'react';

import { Model } from '@avstantso/node-or-browser-js--model-core';

import type { Structure, Context, Routes, Options } from './types';

import { makeRouteInternals } from './make-route-internals';

export function ModelRoute<
  TModel extends Model.Structure,
  TRoutes extends Structure<TModel>,
  TContext extends Context<TModel, TRoutes>,
  TProps extends Routes.Props<TModel, TRoutes, TContext>
>(getOptions?: Options.Get<TModel, TRoutes, TContext, TProps>) {
  const Context = React.createContext<TContext>(undefined);

  const useContext = () => React.useContext(Context);

  function useRoutes(props: TProps) {
    const options: Options<TModel, TRoutes, TContext> = getOptions
      ? getOptions(props)
      : {};

    return makeRouteInternals(Context, props, options);
  }

  return {
    Context,
    useContext,
    useRoutes,
  };
}
