import { Outlet } from 'react-router';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { UrlActions } from '@avstantso/react--data';
import { RouteObject } from '@avstantso/react--components';

export namespace Stub {
  export type Elements = Record<UrlActions.TypeMap['Key'], React.FC>;

  export type Options = {
    allowTrash?: boolean;
    elements?: Elements;
  };
}

export type Stub = {
  <TIdParamName extends string = 'id'>(
    idParamName?: TIdParamName,
    options?: Stub.Options
  ): RouteObject[];
  <TIdParamName extends string = 'id'>(
    idParamName: TIdParamName,
    allowTrash: boolean,
    elements?: Stub.Elements
  ): RouteObject[];
};

export const Stub: Stub = <TParamIdName extends string = 'id'>(
  paramIdName: TParamIdName,
  ...params: any[]
): RouteObject[] => {
  const { allowTrash, elements }: Stub.Options = JS.is.object(params[0])
    ? params[0]
    : { allowTrash: params[0], elements: params[1] };

  const Route = UrlActions._extender(
    (action: UrlActions.Union, custom?: RouteObject): RouteObject => {
      const path = UrlActions.toPath(action);

      const Component = JS.get.raw(elements || {}, UrlActions.toName(action));

      const element = Component ? <Component /> : null;

      return {
        path,
        element,
        ...custom,
      };
    },
    (v, f) => (custom?: RouteObject) => f(v, custom)
  );

  const { Trash, View, Edit, Delete, Restore } = elements || {};

  function makeParamIdRoute(editable?: boolean): RouteObject {
    return {
      path: `:${paramIdName}`,
      element:
        View || (editable ? Edit : Restore) || Delete ? <Outlet /> : null,
      children: [
        Route.View(),
        editable ? Route.Edit() : Route.Restore(),
        Route.Delete(),
      ],
    };
  }

  function makeTrashRoute(): RouteObject {
    return {
      path: UrlActions.Trash.toPath(),
      element: Trash || View || Delete ? <Outlet /> : null,
      children: [Route.Trash({ path: '' }), makeParamIdRoute(false)],
    };
  }
  makeTrashRoute.inArr = () => (allowTrash ? [makeTrashRoute()] : []);

  return [
    Route.Root({
      children: [
        ...makeTrashRoute.inArr(),
        Route.New(),
        makeParamIdRoute(true),
      ],
    }),
  ];
};
