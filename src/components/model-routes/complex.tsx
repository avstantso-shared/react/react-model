import React from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { UrlActions } from '@avstantso/react--data';

import { Types, ModelRoute } from './abstract';

export namespace Complex {
  export type SubRoutes<TSubRoute extends Types.SubRoute> =
    Types.SubRoutes<TSubRoute>;

  export interface Structure<TModel extends Model.Structure>
    extends Types.Structure<TModel> {
    SubRoutes?: SubRoutes<Structure<TModel>['SubRoute']>;
    Dialogs?: {};
  }

  export namespace Context {
    export namespace Dialogs {
      export type Provider<
        TModel extends Model.Structure,
        TRoutes extends Structure<TModel>
      > = {
        dialogs?: TRoutes['Dialogs'];
      };
    }
  }

  export type Context<
    TModel extends Model.Structure,
    TRoutes extends Structure<TModel>
  > = Types.Context<TModel, TRoutes> &
    Context.Dialogs.Provider<TModel, TRoutes>;

  export namespace Routes {
    export namespace Props {
      export type AddNewHook = () => void;
    }

    export type Props<
      TModel extends Model.Structure,
      TRoutes extends Structure<TModel>
    > = Types.Routes.Props<TModel, TRoutes, Context<TModel, TRoutes>> & {
      dialogs?: Perform<TRoutes['Dialogs']>;
    };
  }
}

export function Complex<
  TModel extends Model.Structure,
  TRoutes extends Complex.Structure<TModel> = Complex.Structure<TModel>
>() {
  return ModelRoute<
    TModel,
    TRoutes,
    Complex.Context<TModel, TRoutes>,
    Complex.Routes.Props<TModel, TRoutes>
  >((props) => ({
    rootExt: {
      extendContext: () =>
        !props.dialogs ? {} : { dialogs: Perform(props.dialogs) },
    },
    topViews: +UrlActions.Root | +UrlActions.View | +UrlActions.Edit,
  }));
}
