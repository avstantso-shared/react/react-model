import { useMemo } from 'react';
import { useTranslation } from '@i18n';
import EntityIdIsActioned from './entity-id-is-actioned';

export const useToasts = () => {
  const [t] = useTranslation();

  const entityIdIsActioned = useMemo(() => EntityIdIsActioned(t), [t]);

  return { entityIdIsActioned };
};
