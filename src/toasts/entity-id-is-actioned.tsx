import i18next from 'i18next';

import { JS } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';
import type { Model } from '@avstantso/node-or-browser-js--model-core';

import { Gender, UrlActions } from '@avstantso/react--data';
import { toast } from '@avstantso/react--components';

import { Trans, LOCALES } from '@i18n';
import { EntityName } from '@types';

const LC = LOCALES.entityActions;

export type T = typeof i18next.t;

export default (t: T) =>
  Gender._extender(
    (id: Model.ID, action: UrlActions.Union, entityName?: EntityName.Union) => {
      let entity: string;
      let gender: Gender;
      if (!entityName) {
        entity = t(LOCALES.entity);
        gender = Gender.female;
      } else if (JS.is.string(entityName)) {
        entity = entityName;
        gender = Gender.male;
      } else {
        const o = entityName as EntityName;
        entity = o.value;
        gender = o.gender || Gender.male;
      }

      entity = entity.toCapitalized();

      let localKey: string;
      if (UrlActions.is.New(action)) localKey = LC.inserted;
      else if (UrlActions.is.Edit(action)) localKey = LC.updated;
      else if (UrlActions.is.Delete(action)) localKey = LC.deleted;
      else if (UrlActions.is.Restore(action)) localKey = LC.restored;
      else if (
        (+action & (+UrlActions.Trash | +UrlActions.Edit)) ===
        (+UrlActions.Trash | +UrlActions.Edit)
      )
        localKey = LC.movedToTrash;
      else
        throw new InvalidArgumentError(
          `"${UrlActions.toName(action)}" not supported`
        );

      const localAction = t(localKey, {
        context: Gender.toName(gender),
      });

      return toast.info(
        <Trans.N
          i18nKey={LOCALES.entityIdIsActioned}
          values={{
            entity,
            id,
            action: localAction,
          }}
        />
      );
    },
    (gender, f) =>
      (id: Model.ID, action: UrlActions.Union, entityName: string) =>
        f(id, action, { gender, value: entityName })
  );
