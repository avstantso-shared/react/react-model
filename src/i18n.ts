import { NamesTree } from '@avstantso/node-or-browser-js--utils';

import {
  MakeI18nextResources,
  MakeUseTranslation,
  noTags,
} from '@avstantso/react--components';

import l_en from './locales/en.json';
import l_ru from './locales/ru.json';

export { noTags };

const PACKAGE_JSON = require(`../package.json`); // See @avstantso/node-js--rollup-read-package-json

export const i18next_resources = MakeI18nextResources(
  PACKAGE_JSON.name,
  l_en,
  l_ru
);

export const [useTranslation, Trans] = MakeUseTranslation(PACKAGE_JSON.name);

export const LOCALES = NamesTree.I18ns(l_en).I18n;
