import debug, { Debugger } from 'debug';

import { Extend } from '@avstantso/node-or-browser-js--utils';

type DebugEx = Debugger & {
  subNameSpace<T extends DebugEx>(subNameSpace: string): T;
};

function DebugEx<T extends DebugEx>(prefix: string = ''): T {
  return new Proxy(debug(prefix), {
    get(target, p) {
      return 'subNameSpace' === p
        ? (subNameSpace: string) => DebugEx(`${prefix}${subNameSpace}`)
        : target[p as keyof typeof target];
    },
  }) as T;
}

const dbg = DebugEx(`@avstantso/rm:`);

const model = dbg.subNameSpace(`model:`);
const modelReader = model.subNameSpace(`reader:`);

const pages = dbg.subNameSpace(`pages:`);
const listAsTable = pages.subNameSpace(`listAsTable:`);

export default Extend.Arbitrary(dbg, {
  model: Extend.Arbitrary(model, {
    reader: modelReader,
  }),
  pages: Extend.Arbitrary(pages, {
    listAsTable,
  }),
});
