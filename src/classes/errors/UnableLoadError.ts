import { BaseError, Details } from '@avstantso/node-or-browser-js--errors';
import { isUUIDLike, JS } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';

import { LOCALES as RC_LOCALES } from '@avstantso/react--components';

import { LOCALES } from '@i18n';

const RLC = RC_LOCALES.errors;
const LC = LOCALES.errors;

/**
 * @summary With this error, the page cannot be loaded further
 */
export class UnableLoadError extends BaseError {
  details: Details.Union;

  constructor(message: string, internal?: Error) {
    super(message, internal);

    Object.setPrototypeOf(this, UnableLoadError.prototype);
  }

  static is(error: unknown): error is UnableLoadError {
    return JS.is.error(this, error);
  }

  protected static withId(
    lc: string | object,
    id: Model.ID,
    internal?: Error
  ): UnableLoadError {
    const e = new UnableLoadError(`${lc} "${id}"`, internal);
    e.details = { id };
    return e;
  }

  static invalidId(id: Model.ID, internal?: Error): UnableLoadError {
    return UnableLoadError.withId(
      isUUIDLike(id)
        ? LC['Invalid id UUID-like in URL']
        : LC['Invalid id UUID in URL'],
      id,
      internal
    );
  }

  static systemDataChangeForbidden(
    id: Model.ID,
    internal?: Error
  ): UnableLoadError {
    return UnableLoadError.withId(
      RLC['System data change forbidden'],
      id,
      internal
    );
  }

  static cannotRestoreActive(id: Model.ID, internal?: Error): UnableLoadError {
    return UnableLoadError.withId(
      LC['Cannot restore already active entity'],
      id,
      internal
    );
  }

  protected toLogDetails(): object {
    return {
      ...super.toLogDetails(),
      ...(this.details ? { details: this.details } : {}),
    };
  }
}
