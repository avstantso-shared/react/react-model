import axios from 'axios';
import { AsyncAction } from '@avstantso/react--data';

import './header.scss';

AsyncAction.ignoredErrors.push(axios.isCancel);

export { EntityName } from './types';

export * from './classes';
export * from './history';
export * from './hooks';
export * from './model-storage';
export * from './components';
export * from './toasts';
export * from './i18n';
export * from './utils';
