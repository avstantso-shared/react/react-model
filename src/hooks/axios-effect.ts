import { DependencyList, useEffect } from 'react';
import axios, { AxiosRequestConfig, CancelTokenSource } from 'axios';

import { JS, Canceler } from '@avstantso/node-or-browser-js--utils';

import {
  EffectCallback,
  LateEffect,
  useLateEffect,
} from '@avstantso/react--data';

export namespace AxiosEffect {
  export namespace ActionCallback {
    export type Context = Canceler.Context & {
      config: AxiosRequestConfig;
    };
  }

  export type ActionCallback<TResult = unknown> = (
    context: ActionCallback.Context
  ) => TResult;

  export type CancelCallback = LateEffect.CancelCallback;

  export namespace Use {
    export namespace Late {
      export type Overload = {
        /**
         * @summary Effect: call actionCallback after timeout for axios request if not canceled every render if deps changed
         * @param timeout Timeout in milliseconds to be waited before actionCallback
         * @param actionCallback Callback to call
         * @param cancelCallback Callback for effect cancelation
         * @param deps Effect deps
         * @returns Cancel effect function
         */
        (
          timeout: number,
          actionCallback: ActionCallback,
          cancelCallback?: CancelCallback,
          deps?: DependencyList
        ): LateEffect.Result;

        /**
         * @summary Effect: call actionCallback after timeout for axios request if not canceled every render if deps changed
         * @param timeout Timeout in milliseconds to be waited before actionCallback
         * @param actionCallback Callback to call
         * @param deps Effect deps
         * @returns Cancel effect function
         */
        (
          timeout: number,
          actionCallback: ActionCallback,
          deps?: DependencyList
        ): LateEffect.Result;

        /**
         * @summary Effect: call actionCallback after default timeout for axios request if not canceled every render if deps changed
         * @param actionCallback Callback to call
         * @param cancelCallback Callback for effect cancelation
         * @param deps Effect deps
         * @returns Cancel effect function
         */
        (
          actionCallback: ActionCallback,
          cancelCallback?: CancelCallback,
          deps?: DependencyList
        ): LateEffect.Result;

        /**
         * @summary Effect: call actionCallback after default timeout for axios request if not canceled every render if deps changed
         * @param actionCallback Callback to call
         * @param deps Effect deps
         * @returns Cancel effect function
         */
        (
          actionCallback: ActionCallback,
          deps?: DependencyList
        ): LateEffect.Result;
      };
    }

    export type Overload = {
      /**
       * @summary Effect for axios request with CancelToken
       * @param actionCallback Callback to call
       * @param deps Effect deps
       */
      (
        actionCallback: ActionCallback<ReturnType<EffectCallback>>,
        deps?: DependencyList
      ): void;

      /**
       * @summary Effect: call actionCallback after timeout for axios request if not canceled every render if deps changed
       * @param timeout Timeout in milliseconds to be waited before actionCallback
       * @param actionCallback Callback to call
       * @param deps Effect deps
       * @see `useLateEffect` from `@avstantso/react--data`;
       */
      Late: Late.Overload;
    };
  }
}

const patchContext = (
  context: Canceler.Context,
  cancelToken: CancelTokenSource
) =>
  Object.defineProperties(context as AxiosEffect.ActionCallback.Context, {
    config: {
      value: {
        get cancelToken() {
          return cancelToken.token;
        },
      },
    },
  });

export const useAxiosEffect: AxiosEffect.Use.Overload = (
  actionCallback,
  deps
) => {
  useEffect(() => {
    const canceler = Canceler();
    const cancelToken = axios.CancelToken.source();

    const r = actionCallback(patchContext(canceler.context, cancelToken));

    return () => {
      canceler.cancel();
      cancelToken.cancel();
      JS.is.function(r) && r();
    };
  }, deps);
};

useAxiosEffect.Late = (...params: any[]) => {
  const hasTimeout = JS.is.function(params[0]) ? 0 : 1;
  const hasCancel = Array.isArray(params[1 + hasTimeout]) ? 0 : 1;

  const deps: DependencyList = params[hasTimeout + hasCancel + 1];

  const timeout = hasTimeout ? params[0] : useLateEffect.defaultTimeout;
  const actionCallback: AxiosEffect.ActionCallback = params[hasTimeout];
  const cancelCallback: AxiosEffect.CancelCallback =
    hasCancel && params[hasTimeout + 1];

  let cancelToken: CancelTokenSource;

  return useLateEffect(
    timeout,
    (context) => {
      cancelToken = axios.CancelToken.source();

      return actionCallback(patchContext(context, cancelToken));
    },
    (isActionInvoked) => {
      cancelToken?.cancel();
      cancelCallback && cancelCallback(isActionInvoked);
    },
    deps
  );
};
