import { Model } from '@avstantso/node-or-browser-js--model-core';
import type { API } from '@avstantso/node-or-browser-js--client-lib';

import { ModelStorage as Types } from '@root/types/model-storage';

import { useReadable, useReadableWritable } from './hooks';

import * as Reader from './readers';

export namespace ModelStorage {
  export type Pagination = Types.Pagination;

  export namespace Methods {
    export type Config = Types.Methods.Config;

    export namespace List {
      export namespace Pages {
        export type Search<TModel extends Model.Structure> =
          Types.Methods.List.Pages.Search<TModel>;
      }

      export type Pages<TModel extends Model.Structure> =
        Types.Methods.List.Pages<TModel>;

      export type Search<TModel extends Model.Structure> =
        Types.Methods.List.Search<TModel>;
    }
    export type List<TModel extends Model.Structure> =
      Types.Methods.List<TModel>;
    export type One<TModel extends Model.Structure> = Types.Methods.One<TModel>;
    export type Insert<TModel extends Model.Structure> =
      Types.Methods.Insert<TModel>;
    export type Update<TModel extends Model.Structure> =
      Types.Methods.Update<TModel>;
    export type Delete = Types.Methods.Delete;
  }

  export type Base<
    TModel extends Model.Structure,
    TAPI = API.Axios.Readable<TModel> | API.Axios.Writable<TModel>
  > = Types.Base<TModel, TAPI>;

  export type Readable<TModel extends Model.Structure> = Types.Readable<TModel>;

  export namespace Readable {
    export type Provider<
      TModel extends Model.Structure,
      TModelStorage extends ModelStorage.Readable<TModel>
    > = Types.Readable.Provider<TModel, TModelStorage>;

    export type Options = Types.Readable.Options;
  }

  export type Writable<TModel extends Model.Structure> = Types.Writable<TModel>;

  export namespace Writable {
    export namespace Options {
      export namespace Action {
        export type Props<
          TModel extends Model.Structure,
          TInsOrUpd extends TModel['Insert'] | TModel['Update'] = never
        > = Types.Writable.Options.Action.Props<TModel, TInsOrUpd>;
      }

      /**
       * @returns truthy — prevent default action
       */
      export type Action<
        TModel extends Model.Structure,
        TInsOrUpd extends TModel['Insert'] | TModel['Update'] = never
      > = Types.Writable.Options.Action<TModel, TInsOrUpd>;
    }

    export type Options<TModel extends Model.Structure> =
      Types.Writable.Options<TModel>;
  }

  export type ReadableWritable<TModel extends Model.Structure> =
    Types.ReadableWritable<TModel>;

  export namespace ReadableWritable {
    export type Provider<
      TModel extends Model.Structure,
      TModelStorage extends ReadableWritable<TModel>
    > = Types.ReadableWritable.Provider<TModel, TModelStorage>;
  }

  export type Union<TModel extends Model.Structure> = Types.Union<TModel>;

  export namespace Union {
    export type Provider<
      TModel extends Model.Structure,
      TModelStorage extends Union<TModel>
    > = Types.Union.Provider<TModel, TModelStorage>;
  }

  export namespace Reader {
    export type Pagination = Types.Reader.Pagination;

    export namespace Base {
      export type Options<TResult> = Types.Reader.Base.Options<TResult>;
    }

    export namespace List {
      export type Options<TModel extends Model.Structure> =
        Types.Reader.List.Options<TModel>;
    }

    export namespace One {
      export type Options<TModel extends Model.Structure> =
        Types.Reader.One.Options<TModel>;
    }
  }
}

export const ModelStorage = {
  Readable: { use: useReadable },
  ReadableWritable: { use: useReadableWritable },
  Reader,
};
