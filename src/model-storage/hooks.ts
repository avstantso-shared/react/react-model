import React, { useEffect, useMemo, useRef } from 'react';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { API } from '@avstantso/node-or-browser-js--client-lib';

import { DataCollection, useDataCollection } from '@avstantso/react--data';

import type { ModelStorage } from '@types';

function empty<
  TModel extends Model.Structure,
  R extends ModelStorage.Union<TModel>
>(readOnly?: boolean): R {
  return Generics.Cast.To({
    isReady: null,
    name: null,
    collection: null,
    api: null,
    list: null,
    one: null,
    ...(!readOnly
      ? {
          insert: null,
          update: null,
          delete: null,
        }
      : {}),
  });
}

function useCollection<TModel extends Model.Structure>(
  api: API.Axios.Readable<TModel>,
  label: string,
  options?: ModelStorage.Readable.Options
): DataCollection<TModel['Select']> {
  const { desc } = options || {};

  const collection = useDataCollection<TModel['Select']>({
    label,
    externalDeps: [api],
    unique: true,
    desc,
  });

  useEffect(() => {
    collection.clear();
  }, [api]);

  return collection;
}

function initReadMethods<TModel extends Model.Structure>(
  current: ModelStorage.Readable<TModel>,
  options: ModelStorage.Readable.Options
): void {
  const listSearch: ModelStorage.Methods.List.Search<TModel> = (
    search,
    config?
  ) =>
    current.api.list.search(search, config).tap((items) => {
      const { collection } = current;

      collection.setter(items);
      collection.setTotal(items.length);
    });

  const list: ModelStorage.Methods.List<TModel> = (config?) =>
    listSearch(undefined, config);

  const listPagesSearch: ModelStorage.Methods.List.Pages.Search<TModel> = (
    pagination,
    search,
    config?
  ) => {
    const { num, size, clear } = pagination;
    return current.api.list.pages
      .search({ num, size }, search, config)
      .tap(([total, items]) => {
        const { collection } = current;

        clear ? collection.setter(items) : collection.addOrSet(...items);

        collection.setTotal(total);
      });
  };

  const listPages: ModelStorage.Methods.List.Pages<TModel> = (
    pagination,
    config?
  ) => listPagesSearch(pagination, undefined, config);

  listPages.search = listPagesSearch;
  list.search = listSearch;
  list.pages = listPages;

  const one: ModelStorage.Methods.One<TModel> = (id, config?) =>
    current.api.one(id, config).tap(current.collection.replace);

  current.list = list;
  current.one = one;
}

function initWriteMethods<TModel extends Model.Structure>(
  current: ModelStorage.ReadableWritable<TModel>,
  options: ModelStorage.Writable.Options<TModel>
): void {
  const { inserting, updating, deleting } = options || {};

  const insert: ModelStorage.Methods.Insert<TModel> = (entity, config?) =>
    current.api.insert(entity, config).tap(async (fromServer) => {
      const handled: boolean =
        inserting &&
        (await Promise.resolve(
          inserting({ entity, fromServer, collection: current.collection })
        ));

      !handled && current.collection.add(fromServer);
    });

  const update: ModelStorage.Methods.Update<TModel> = (entity, config?) =>
    current.api.update(entity, config).tap(async (fromServer) => {
      const handled: boolean =
        updating &&
        (await Promise.resolve(
          updating({ entity, fromServer, collection: current.collection })
        ));

      !handled && current.collection.replace(fromServer);
    });

  const _delete: ModelStorage.Methods.Delete = (id, config?) =>
    current.api
      .delete(id, config)
      .tap(async (isOk) => {
        if (!isOk) return;

        const handled: boolean =
          deleting &&
          (await Promise.resolve(
            deleting({
              entity: current.collection
                .by(Generics.Cast(Model.Fields.id))
                .find(id),
              fromServer: undefined,
              collection: current.collection,
            })
          ));

        !handled && current.collection.delete(id);
      })
      .then((idFromServer) => !!idFromServer);

  current.insert = insert;
  current.update = update;
  current.delete = _delete;
}

function useModelStorage<
  TModel extends Model.Structure,
  A extends API.Axios.Union<TModel>,
  R extends ModelStorage.Union<TModel>
>(
  model: Model.Declaration<TModel>,
  api: A,
  options: ModelStorage.Writable.Options<TModel>,
  readOnly?: boolean
): R {
  const ref = useRef<R>();
  if (!ref.current) {
    ref.current = {} as R;

    initReadMethods<TModel>(ref.current, options);

    if (!readOnly)
      initWriteMethods<TModel>(Generics.Cast(ref.current), options);
  }
  ref.current.api = api;
  ref.current.collection = useCollection(api, model.name, options);

  return useMemo<R>(
    () => ({
      ...ref.current,
      get isReady() {
        return !!ref.current.api;
      },
      get name() {
        return ref.current.collection.label;
      },
      get collection() {
        return ref.current.collection;
      },
      get api() {
        return ref.current.api;
      },
    }),
    [ref.current.collection, model.name]
  );
}

export function useReadable<TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  api: API.Axios.Readable<TModel>,
  options?: ModelStorage.Readable.Options
): ModelStorage.Readable<TModel> {
  return useModelStorage(model, api, options, true);
}

useReadable.empty = <TModel extends Model.Structure>() =>
  empty<TModel, ModelStorage.Readable<TModel>>(true);

export function useReadableWritable<TModel extends Model.Structure>(
  model: Model.Declaration<TModel>,
  api: API.Axios.ReadableWritable<TModel>,
  options?: ModelStorage.Writable.Options<TModel>
): ModelStorage.ReadableWritable<TModel> {
  return useModelStorage(model, api, options, false);
}

useReadableWritable.empty = <TModel extends Model.Structure>() =>
  empty<TModel, ModelStorage.ReadableWritable<TModel>>(false);
