import React from 'react';
import { validate as isValidUUID } from 'uuid';

import { Generics } from '@avstantso/node-or-browser-js--utils';
import { Model } from '@avstantso/node-or-browser-js--model-core';
import { AsyncAction } from '@avstantso/react--components';

import type { ModelStorage } from '@types';
import { UnableLoadError } from '@classes';
import dbg from '@debug';
import { useAxiosEffect } from '@hooks';

/**
 * @summary Reader of one entity
 * @param modelStorage
 * @param id Entity id.
 *   undefined === id: Maybe reloaded on next renders /
 *   null === id: Not reloaded on next renders
 * @param options
 */
export function useOne<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.Readable<TModel>
>(
  modelStorage: TModelStorage,
  id: Model.ID,
  options?: ModelStorage.Reader.One.Options<TModel>
) {
  const {
    setResult,
    asyncAction = {} as AsyncAction.Dispatch,
    noReload,
    source,
    disabled,
    late,
    label,
  } = options || {};

  const Exec = AsyncAction.Exec(asyncAction);

  (late ? useAxiosEffect.Late : useAxiosEffect)(
    ({ safe, config }) => {
      if (!modelStorage.isReady || disabled || undefined === id) return;

      const result = safe(setResult);
      const exec = Exec(safe);

      if (null === id) {
        exec(async () => null);
        return;
      }

      if (!isValidUUID(id)) {
        exec(async () => {
          throw UnableLoadError.invalidId(id);
        });

        return;
      }

      if (source) {
        exec(async () => {
          modelStorage.collection.addOrSet(source);
          result(source);
          dbg.model.reader.subNameSpace(modelStorage.name)(
            'list readed from source %O',
            source
          );
        });

        return;
      }

      if (
        noReload &&
        modelStorage.collection.by(Generics.Cast(Model.Fields.id)).find(id)
      )
        return;

      exec(
        () => modelStorage.one(id, config),
        (data) => {
          result(data);
          dbg.model.reader.subNameSpace(modelStorage.name)(
            `one (id: ${id}) readed from server %O`,
            data
          );
        }
      );
    },
    [modelStorage.api, id, source, disabled]
  );
}
