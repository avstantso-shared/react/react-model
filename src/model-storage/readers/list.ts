import React from 'react';
import Bluebird from 'bluebird';
import axios from 'axios';

import { Model } from '@avstantso/node-or-browser-js--model-core';
import { AsyncAction, useViewport } from '@avstantso/react--components';

import type { ModelStorage } from '@types';
import dbg from '@debug';
import { useAxiosEffect } from '@hooks';

/**
 * @summary Entity list reader
 * @param modelStorage
 * @param options
 */
export function useList<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.Readable<TModel>
>(
  modelStorage: TModelStorage,
  options?: ModelStorage.Reader.List.Options<TModel>
) {
  const {
    search,
    pagination,
    setResult,
    asyncAction = {} as AsyncAction.Dispatch,
    disabled,
    noReload,
    source,
    late,
    label,
  } = options || {};

  const Exec = AsyncAction.Exec(asyncAction);

  (late ? useAxiosEffect.Late : useAxiosEffect)(
    ({ safe, config }) => {
      if (!modelStorage.isReady || disabled) return;

      const result = safe(setResult);
      const exec = Exec(safe);

      if (source) {
        exec(async () => {
          modelStorage.collection.setter(source);
          result(source);
          dbg.model.reader.subNameSpace(modelStorage.name)(
            'list readed from source %O',
            source
          );
        });

        return;
      }

      if (noReload && modelStorage.collection.length) return;

      const fromListSearch = async () =>
        exec(
          () => modelStorage.list.search(search, config),
          (data) => {
            result(data);
            dbg.model.reader.subNameSpace(modelStorage.name)(
              'list readed from server %O',
              data
            );
          }
        );

      function applyPagesResult(pg: Model.Pagination) {
        const { num, size } = pg;

        return (pr: Model.Pagination.Result<TModel>) => {
          const [total, data] = pr;

          result(data);

          dbg.model.reader.subNameSpace(modelStorage.name)(
            `list.pages #${num} with size ${size} readed from server (total: ${total}) %O`,
            data
          );

          return pr;
        };
      }

      const fromListPagesSearch = async () =>
        exec(
          () => modelStorage.list.pages.search(pagination, search, config),
          applyPagesResult(pagination)
        );

      async function loadPagesIterable() {
        const { num, size, clear } = pagination;

        const first = async () =>
          exec(
            () =>
              modelStorage.list.pages.search(
                { num, size, clear },
                search,
                config
              ),
            applyPagesResult({ num, size })
          );

        function recursion(num: number) {
          return safe(
            async (
              prev: Model.Pagination.Result<TModel>
            ): Promise<Model.Pagination.Result<TModel>> => {
              const [prevTotal] = prev;
              if (size * num >= prevTotal) return prev;

              const pr = await modelStorage.list.pages
                .search({ num, size }, search, config)
                .then(applyPagesResult({ num, size }));

              return await recursion(num + 1)(pr);
            }
          );
        }

        return first()
          .then(recursion(num + 1))
          .catch(safe(asyncAction.setLastError));
      }

      !pagination
        ? fromListSearch()
        : !pagination.iterable
        ? fromListPagesSearch()
        : loadPagesIterable();
    },
    [
      modelStorage.api,
      source,
      disabled,
      search,
      pagination?.num,
      pagination?.size,
      pagination?.clear,
      pagination?.iterable,
    ]
  );
}
