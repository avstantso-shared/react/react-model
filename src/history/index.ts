import type { History as Types } from '@types';

import { HistoryContext as Context, useHistory } from './context';
import { HistoryContextProvider as Provider } from './provider';

export { useHistory };

export namespace History {
  export namespace Context {
    export namespace IsFrom {
      export type IDCallback = Types.Context.IsFrom.IDCallback;
    }

    export type IsFrom = Types.Context.IsFrom;

    export namespace SmartBack {
      export type Options = Types.Context.SmartBack.Options;
    }

    export type SmartBack = Types.Context.SmartBack;
  }

  export type Context = Types.Context;

  export namespace Provider {
    export type Props = Types.Context.Provider.Props;
  }
}

export const History = { Context, use: useHistory, Provider };
