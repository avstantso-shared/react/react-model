import React, { useMemo } from 'react';

import { Perform } from '@avstantso/node-or-browser-js--utils';
import { InvalidArgumentError } from '@avstantso/node-or-browser-js--errors';

import type { History } from '@types';

import { HistoryContext } from './context';
import { From } from './from';
import { IsFrom } from './is-from';

export const HistoryContextProvider: React.FC<
  History.Context.Provider.Props
> = (props) => {
  const { queue, children } = props;

  const smartBack =
    props.smartBack ||
    (() => {
      throw new InvalidArgumentError(
        'smartBack must be passed as prop into HistoryContextProvider'
      );
    });

  const [from, isFrom] = useMemo(() => [From(queue), IsFrom(queue)], [queue]);

  return (
    <HistoryContext.Provider value={{ queue, from, isFrom, smartBack }}>
      {Perform(children)}
    </HistoryContext.Provider>
  );
};
