import { useMemo } from 'react';

import { UrlActions } from '@avstantso/react--data';

import type { History } from '@types';

type Queue = History.Context.Queue;

const empty = new Map<string, UrlActions.RouteItem>();
const fromEmpty = () => empty;

export function From(queue: Queue): History.Context.From {
  return queue.length < 2
    ? fromEmpty
    : () => {
        const from = queue[1].location.pathname;
        return UrlActions.parsePath(from);
      };
}

From.use = (queue: Queue) => useMemo(() => From(queue), [queue]);
