import { useMemo } from 'react';
import { validate as isValidUUID } from 'uuid';

import { JS } from '@avstantso/node-or-browser-js--utils';

import { UrlActions } from '@avstantso/react--data';

import type { History } from '@types';

type Queue = History.Context.Queue;
type Options = History.Context.IsFrom.Options;
type IDCallback = History.Context.IsFrom.IDCallback;

export function IsFrom(queue: Queue): History.Context.IsFrom {
  const isFrom =
    queue.length < 2
      ? () => false
      : (...params: any[]) => {
          const from = queue[1].location.pathname;

          if (JS.is.string(params[0])) return params[0] === from;

          const action: UrlActions.Union = params.shift();

          const pathname: string = JS.switch.extract(params, {
            string: (p) => p,
            object: (p: Options) => p.pathname,
          });

          const callback: IDCallback = JS.switch.extract(params, {
            function: (p: any) => p,
            object: (p: Options) => p.callback,
          });

          const parts = UrlActions.extractPathParts(from, pathname);

          const current = UrlActions.fromPathParts(parts);

          if (!(+current & +action)) return false;

          if (!(+action & +UrlActions.WithId)) return true;

          const id = current.idFromPathParts(parts);

          return id && isValidUUID(id) && (!callback || !!callback(id));
        };

  return UrlActions._extender(
    isFrom,
    (v, f) =>
      (...params: any[]) =>
        f(v, ...params)
  );
}

IsFrom.use = (queue: Queue) => useMemo(() => IsFrom(queue), [queue]);
