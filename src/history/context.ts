import { createContext, useContext } from 'react';

import type { History } from '@types';

const empty: History.Context = {
  queue: null,
  from: null,
  isFrom: null,
  smartBack: null,
};

export const HistoryContext = createContext<History.Context>(empty);
export const useHistory = () => useContext(HistoryContext);
