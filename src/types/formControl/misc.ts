import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { AlternateJSX } from '@avstantso/react--data';

export type GetCaption<TModel extends Model.Structure> =
  AlternateJSX.Getter.Check<
    [TModel['Select']],
    <TStrictString extends boolean = undefined>(
      item: TModel['Select'],
      strictString?: TStrictString
    ) => AlternateJSX.Parse<TStrictString>
  >;

export type FindByValue<TModel extends Model.Structure> = (
  text: string
) => TModel['Select'];
