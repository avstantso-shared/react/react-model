import type React from 'react';

import type {
  FormControlProps,
  ButtonProps,
  DropdownButtonProps,
} from 'react-bootstrap';

import type { Model } from '@avstantso/node-or-browser-js--model-core';

import type { ModelStorage } from '../model-storage';

import type { GetCaption, FindByValue } from './misc';

export namespace Props {
  export type Visual = FormControlProps &
    Pick<ButtonProps, 'variant' | 'autoFocus'> & {
      /**
       * @summary dom element id
       */
      id?: ButtonProps['id'];

      dropdownAlign?: DropdownButtonProps['align'];
      value: string;
      feedback?: string;
    };
}

export type Props<
  TModel extends Model.Structure,
  TModelStorage extends ModelStorage.ReadableWritable<TModel>
> = ModelStorage.ReadableWritable.Provider<TModel, TModelStorage> &
  Props.Visual & {
    getCaption?: GetCaption<TModel>;
    findByValue?: FindByValue<TModel>;
    setValue?(text: string): void;
    onEntityChange?(entity: TModel['Select']): void;
    filter?(item: TModel['Select']): boolean;
    leftChildren?: React.ReactNode | React.ReactNode[];
    rightChildren?: React.ReactNode | React.ReactNode[];
  };
