import { Gender } from '@avstantso/react--data';

export namespace EntityName {
  export type Union = string | EntityName;

  export type Initializer = {
    // @ts-ignore fix CI TS2589: Type instantiation is excessively deep and possibly infinite
    (value: string, gender: Gender): EntityName;
    (value: string): string;
  };
}

export interface EntityName {
  value: string;
  gender?: Gender;
}

export const EntityName = Gender._extender.Capitalized(
  ((value: string, gender?: Gender): EntityName.Union =>
    gender ? { value, gender } : value) as EntityName.Initializer,
  (g, f) => (value: string) => f(value, g) as EntityName
);
