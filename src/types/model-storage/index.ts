import type { Dispatch, SetStateAction } from 'react';
import type { AxiosRequestConfig } from 'axios';

import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { API } from '@avstantso/node-or-browser-js--client-lib';
import type { DataCollection, DebugLabel } from '@avstantso/react--data';
import type { AsyncAction } from '@avstantso/react--components';

export namespace ModelStorage {
  export type Pagination = Model.Pagination & { clear?: boolean };

  export namespace Methods {
    export type Config = AxiosRequestConfig;

    export namespace List {
      export namespace Pages {
        export type Search<TModel extends Model.Structure> = (
          pagination: Pagination,
          search: TModel['Condition'],
          config?: Config
        ) => Promise<Model.Pagination.Result<TModel>>;
      }

      export type Pages<TModel extends Model.Structure> = {
        (pagination: Pagination, config?: Config): Promise<
          Model.Pagination.Result<TModel>
        >;
        search: Pages.Search<TModel>;
      };

      export type Search<TModel extends Model.Structure> = (
        search: TModel['Condition'],
        config?: Config
      ) => Promise<TModel['Select'][]>;
    }

    export type List<TModel extends Model.Structure> = {
      (config?: Config): Promise<TModel['Select'][]>;

      search: List.Search<TModel>;

      /**
       * @summary Query for page with number 0 clear previous list
       */
      pages: List.Pages<TModel>;
    };

    export type One<TModel extends Model.Structure> = (
      id: Model.ID,
      config?: Config
    ) => Promise<TModel['Select']>;

    export type Insert<TModel extends Model.Structure> = (
      entity: TModel['Insert'] | TModel['Select'],
      config?: Config
    ) => Promise<TModel['Select']>;

    export type Update<TModel extends Model.Structure> = (
      entity: TModel['Update'] | TModel['Select'],
      config?: Config
    ) => Promise<TModel['Select']>;

    export type Delete = (id: Model.ID, config?: Config) => Promise<boolean>;
  }

  export type Base<
    TModel extends Model.Structure,
    TAPI = API.Axios.Readable<TModel> | API.Axios.Writable<TModel>
  > = {
    readonly isReady: boolean;
    readonly name: string;
    collection: DataCollection<TModel['Select']>;
    api: TAPI;
  };

  export type Readable<TModel extends Model.Structure> = Base<
    TModel,
    API.Axios.Readable<TModel>
  > & {
    list: Methods.List<TModel>;
    one: Methods.One<TModel>;
  };

  export namespace Readable {
    export type Provider<
      TModel extends Model.Structure,
      TModelStorage extends ModelStorage.Readable<TModel>
    > = Union.Provider<TModel, TModelStorage>;

    export type Options = Pick<DataCollection.Options<any>, 'desc'>;
  }

  export type Writable<TModel extends Model.Structure> = Base<
    TModel,
    API.Axios.Readable<TModel>
  > & {
    insert: Methods.Insert<TModel>;
    update: Methods.Update<TModel>;
    delete: Methods.Delete;
  };

  export namespace Writable {
    export namespace Options {
      export namespace Action {
        export type Props<
          TModel extends Model.Structure,
          TInsOrUpd extends TModel['Insert'] | TModel['Update'] = never
        > = {
          entity: TInsOrUpd | TModel['Select'];
          fromServer: TModel['Select'];
          collection: DataCollection<TModel['Select']>;
        };
      }

      /**
       * @returns truthy — prevent default action
       */
      export type Action<
        TModel extends Model.Structure,
        TInsOrUpd extends TModel['Insert'] | TModel['Update'] = never
      > = (
        props: Action.Props<TModel, TInsOrUpd>
      ) => boolean | Promise<boolean>;
    }

    export type Options<TModel extends Model.Structure> = Readable.Options & {
      inserting?: Options.Action<TModel, TModel['Insert']>;
      updating?: Options.Action<TModel, TModel['Update']>;
      deleting?: Options.Action<TModel>;
    };
  }

  export type ReadableWritable<TModel extends Model.Structure> =
    Readable<TModel> &
      Writable<TModel> & {
        api: API.Axios.ReadableWritable<TModel>;
      };

  export namespace ReadableWritable {
    export type Provider<
      TModel extends Model.Structure,
      TModelStorage extends ReadableWritable<TModel>
    > = Union.Provider<TModel, TModelStorage>;
  }

  export type Union<TModel extends Model.Structure> =
    | Readable<TModel>
    | ReadableWritable<TModel>;

  export namespace Union {
    export type Provider<
      TModel extends Model.Structure,
      TModelStorage extends Union<TModel>
    > = {
      modelStorage: TModelStorage;
    };
  }

  export namespace Reader {
    export type Pagination = ModelStorage.Pagination & { iterable?: boolean };

    export namespace Base {
      export type Options<TResult> = Readonly<
        DebugLabel.Option &
          Partial<AsyncAction.Dispatch.Provider> & {
            setResult?: Dispatch<SetStateAction<TResult>>;
            disabled?: boolean;
            noReload?: boolean;
            source?: TResult;
            late?: boolean;
          }
      >;
    }

    export namespace List {
      export type Options<TModel extends Model.Structure> = Base.Options<
        TModel['Select'][]
      > &
        Model.Select.Options<TModel, Pagination>;
    }

    export namespace One {
      export type Options<TModel extends Model.Structure> = Base.Options<
        TModel['Select']
      >;
    }
  }
}
