import type { Update } from 'history';
import type { To } from 'react-router';

import type { Model } from '@avstantso/node-or-browser-js--model-core';
import type { ChildrenProps, UrlActions } from '@avstantso/react--data';

export namespace Context {
  export namespace Queue {
    export type Provider = { queue: Queue };
  }

  export type Queue = ReadonlyArray<Update>;

  export namespace From {
    export type Provider = {
      from: From;
    };
  }

  export type From = () => UrlActions.RouteItems;

  export namespace IsFrom {
    export type IDCallback = (id: Model.ID) => boolean | any;

    export type Options = { pathname?: string; callback?: IDCallback };

    export namespace Action {
      export type Full = {
        (urlAction: UrlActions.Union, options?: Options): boolean;
        (
          urlAction: UrlActions.Union,
          callback?: IsFrom.IDCallback,
          pathname?: string
        ): boolean;
        (
          urlAction: UrlActions.Union,
          pathname?: string,
          callback?: IsFrom.IDCallback
        ): boolean;
      };

      export type WithoutId = {
        (options?: Omit<Options, 'callback'>): boolean;
        (pathname?: string): boolean;
      };

      export type WithId = {
        (options?: Options): boolean;
        (callback?: IsFrom.IDCallback, pathname?: string): boolean;
        (pathname?: string, callback?: IsFrom.IDCallback): boolean;
      };

      export namespace Rec {
        export type WithoutId = Record<
          Exclude<UrlActions.WithPath, UrlActions.WithId>,
          Action.WithoutId
        >;

        export type WithId = Record<UrlActions.WithId, Action.WithId>;
      }

      export type Rec = Rec.WithoutId & Rec.WithId;
    }

    export type Provider = {
      isFrom: IsFrom;
    };
  }

  export type IsFrom = { (url: string): boolean } & IsFrom.Action.Full &
    IsFrom.Action.Rec;

  export namespace SmartBack {
    export type Options = {
      doNotGo?: boolean;
    };

    export type Provider = {
      smartBack: SmartBack;
    };
  }

  export type SmartBack = (options?: SmartBack.Options) => To;

  export namespace Provider {
    export type Props = ChildrenProps.Perform &
      Queue.Provider &
      Partial<SmartBack.Provider>;
  }
}

export type Context = Context.Queue.Provider &
  Context.SmartBack.Provider &
  Context.From.Provider &
  Context.IsFrom.Provider;
